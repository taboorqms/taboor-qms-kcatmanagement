package com.taboor.qms.kcat.serviceImpl;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.taboor.qms.core.auth.MyUserDetails;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.AgentCounterConfig;
import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.BranchCounter;
import com.taboor.qms.core.model.BranchCounterAgentMapper;
import com.taboor.qms.core.model.BranchCounterConfig;
import com.taboor.qms.core.model.BranchTVConfig;
import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.model.Ticket;
import com.taboor.qms.core.model.User;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.HttpHeadersUtils;
import com.taboor.qms.core.utils.NotificationSender;
import com.taboor.qms.core.utils.PushNotification;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.core.utils.RoleName;
import com.taboor.qms.kcat.payload.AddCounterConfigPayload;
import com.taboor.qms.kcat.payload.AddTVConfigPayload;
import com.taboor.qms.kcat.payload.ChangeCounterStatusPayload;
import com.taboor.qms.kcat.payload.GetAllCounterTicketsPayload;
import com.taboor.qms.kcat.payload.GetCounterTicketPayload;
import com.taboor.qms.kcat.payload.NewCounterPayload;
import com.taboor.qms.kcat.payload.TicketChangeCounterNotifPayload;
import com.taboor.qms.kcat.response.AddCounterConfigResponse;
import com.taboor.qms.kcat.response.GetBranchAdminCounterListResponse;
import com.taboor.qms.kcat.response.GetCountersResponse;
import com.taboor.qms.kcat.response.GetTicketCounterResponse;
import com.taboor.qms.kcat.service.KcatService;

@Service
public class KcatServiceImpl implements KcatService {

//	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(KcatServiceImpl.class);
    @Value("${url.microservice.db.connector}")
    private String dbConnectorMicroserviceURL;

    @Value("${url.microservice.user.management}")
    private String userManagementMicroserviceURL;

    @Override
    public List<GetBranchAdminCounterListResponse> getBranchByEmp() throws TaboorQMSServiceException, Exception {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        User user = session.getUser();

        AgentCounterConfig agentCounterConfigEntity = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                + "/tab/agentcounterconfigs/get/token?deviceToken=" + session.getDeviceToken(),
                AgentCounterConfig.class);
        BranchCounter branchCounterEntity = null;
        if (agentCounterConfigEntity != null) {
            branchCounterEntity = RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/branchcounters/get/id?branchCounterId="
                    + agentCounterConfigEntity.getBranchCounter().getCounterId(),
                    BranchCounter.class, xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorCode(),
                    xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorDetails());
        }

        List<?> getBranchListResponse = RestUtil.get(dbConnectorMicroserviceURL
                + "/tab/employeebranchmapper/getBranch/user?userId=" + session.getUser().getUserId(), List.class);
        List<Branch> branchList = GenericMapper.convertListToObject(getBranchListResponse,
                new TypeReference<List<Branch>>() {
        });

        List<GetBranchAdminCounterListResponse> branchAdminCounters = new ArrayList<GetBranchAdminCounterListResponse>();
        
        for (Branch branch : branchList) {
            GetBranchAdminCounterListResponse branchAdminBranch = new GetBranchAdminCounterListResponse();
            branchAdminBranch.setBranchId(branch.getBranchId());
            branchAdminBranch.setBranchName(branch.getBranchName());
            branchAdminBranch.setBranchNameArabic(branch.getBranchNameArabic());
            branchAdminBranch.setEmailAddress(branch.getEmailAddress());

            List<?> getBranchCounterListResponse = RestUtil.get(
                    dbConnectorMicroserviceURL + "/tab/branchcounters/get/branchId?branchId=" + branch.getBranchId(),
                    List.class);
            List<BranchCounter> branchCounterList = GenericMapper.convertListToObject(getBranchCounterListResponse,
                    new TypeReference<List<BranchCounter>>() {
            });
            
            List<GetBranchAdminCounterListResponse.BranchAdminCounter> branchAdminCounterList = new ArrayList<GetBranchAdminCounterListResponse.BranchAdminCounter>();
            for (BranchCounter branchCounter : branchCounterList) {
                GetBranchAdminCounterListResponse.BranchAdminCounter branchAdminCounter = new GetBranchAdminCounterListResponse().new BranchAdminCounter();
                branchAdminCounter.setCounterId(branchCounter.getCounterId());
                branchAdminCounter.setCounterNumber(branchCounter.getCounterNumber());
                if(user.getRoleName().equals(RoleName.Branch_Admin.name())){
                    branchAdminCounter.setSetup(true);
                    branchAdminCounterList.add(branchAdminCounter);
                } else{
                    if (branchCounterEntity != null && branchCounterEntity.getCounterNumber().equals(branchCounter.getCounterNumber())) {
                        branchAdminCounter.setSetup(true);
                        branchAdminCounterList.add(branchAdminCounter);
                    }
                }
//                Counters who have been assigned some agent are returned only.
//                else {
//                    branchAdminCounter.setSetup(false);
//                }

            }
            branchAdminBranch.setCounterList(branchAdminCounterList);
            branchAdminBranch.setApplicationStatusCode(xyzResponseCode.SUCCESS.getCode());
            branchAdminBranch.setApplicationStatusResponse(xyzResponseMessage.BRANCH_COUNTER_FETCHED.getMessage());
            branchAdminCounters.add(branchAdminBranch);
        }
        return branchAdminCounters;
    }

    @Override
    public AddCounterConfigResponse addCounterConfig(AddCounterConfigPayload addCounterConfigPayload)
            throws TaboorQMSServiceException, Exception {

        BranchCounter branchCounter = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounters/get/id?branchCounterId="
                + addCounterConfigPayload.getBranchCounterId(),
                BranchCounter.class, xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorDetails());

        BranchCounterConfig branchCounterConfigResponse = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                + "/tab/branchcounterconfigs/get/counter?branchCounterId=" + branchCounter.getCounterId(),
                BranchCounterConfig.class);

        if (branchCounterConfigResponse != null) {
            if (!branchCounterConfigResponse.getDeviceToken().equals(addCounterConfigPayload.getDeviceToken())) {
                throw new TaboorQMSServiceException(xyzErrorMessage.COUNTERCONFIG_ALREADY_EXISTS.getErrorCode() + "::"
                        + xyzErrorMessage.COUNTERCONFIG_ALREADY_EXISTS.getErrorDetails());
            } else {
                branchCounterConfigResponse.setFeedBackScreen(addCounterConfigPayload.getFeedBackScreen());
                RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/branchcounterconfigs/save",
                        new HttpEntity<>(branchCounterConfigResponse, HttpHeadersUtils.getApplicationJsonHeader()),
                        BranchCounterConfig.class, xyzErrorMessage.COUNTERCONFIG_NOT_CREATED.getErrorCode(),
                        xyzErrorMessage.COUNTERCONFIG_NOT_CREATED.getErrorDetails());
            }

        } else {
            BranchCounterConfig branchCounterConfig = new BranchCounterConfig();
            branchCounterConfig.setBranchCounter(branchCounter);
            branchCounterConfig.setDeviceToken(addCounterConfigPayload.getDeviceToken());
            branchCounterConfig.setFeedBackScreen(addCounterConfigPayload.getFeedBackScreen());

            HttpEntity<BranchCounterConfig> entity = new HttpEntity<>(branchCounterConfig,
                    HttpHeadersUtils.getApplicationJsonHeader());
            RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/branchcounterconfigs/save", entity,
                    BranchCounterConfig.class, xyzErrorMessage.COUNTERCONFIG_NOT_CREATED.getErrorCode(),
                    xyzErrorMessage.COUNTERCONFIG_NOT_CREATED.getErrorDetails());
        }
        BranchCounterAgentMapper counterAgentMapper = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                + "/tab/branchcounteragent/get/counter?counterId=" + branchCounter.getCounterId(),
                BranchCounterAgentMapper.class);

        if (counterAgentMapper == null) {
            return new AddCounterConfigResponse(xyzResponseCode.SUCCESS.getCode(),
                    xyzResponseMessage.COUNTER_CONFIG_ADDED.getMessage(), false);
        } else {
            return new AddCounterConfigResponse(xyzResponseCode.SUCCESS.getCode(),
                    xyzResponseMessage.COUNTER_CONFIG_ADDED.getMessage(),
                    counterAgentMapper.getAgent().isLoginStatus());
        }
    }

    @Override
    public GetTicketCounterResponse getCounterTicket(GetCounterTicketPayload getCounterTicketPayload)
            throws TaboorQMSServiceException, Exception {

        BranchCounter branchCounter = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounters/get/id?branchCounterId="
                + getCounterTicketPayload.getBranchCounterId(),
                BranchCounter.class, xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorDetails());

        Ticket ticket = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/tickets/getTicket/tickerId?branchId="
                + branchCounter.getBranch().getBranchId() + "&counterNo=" + branchCounter.getCounterNumber(),
                Ticket.class, xyzErrorMessage.TICKET_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.TICKET_NOT_EXISTS.getErrorDetails());

        return new GetTicketCounterResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.COUNTER_CONFIG_ADDED.getMessage(), ticket.getTicketNumber(),
                ticket.getCounterNumber());
    }

    // Test Notifications
    @Override
    public GenStatusResponse sendTicketChange(GetCounterTicketPayload getCounterTicketPayload)
            throws TaboorQMSServiceException, Exception {

        BranchCounter branchCounter = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounters/get/id?branchCounterId="
                + getCounterTicketPayload.getBranchCounterId(),
                BranchCounter.class, xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorDetails());

        Random random = new Random();
        String ticketN = String.format("%04d", random.nextInt(10000));

        // send notification to counter application to change the serving ticket
        RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounterconfigs/get/counter?branchCounterId="
                + branchCounter.getCounterId(),
                BranchCounterConfig.class, xyzErrorMessage.COUNTERCONFIG_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.COUNTERCONFIG_NOT_EXISTS.getErrorDetails());

        Map<String, String> metaData = new HashMap<String, String>();
        metaData.put("NotificationCode", PushNotification.TICKET_SERVING_COUNTER.getNotificationCode());
        metaData.put("ticketNumber", "M" + ticketN);
        metaData.put("counterNumber", branchCounter.getCounterNumber());

        NotificationSender.sendPushNotificationData(PushNotification.TICKET_SERVING_COUNTER.getNotificationTitle(),
                PushNotification.TICKET_SERVING_COUNTER.getNotificationDescription(),
                getCounterTicketPayload.getDeviceToken(), metaData);

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.COUNTER_CONFIG_ADDED.getMessage());
    }

    // Test Notifications
    @Override
    public GenStatusResponse sendFeedbackGet(GetCounterTicketPayload getCounterTicketPayload)
            throws TaboorQMSServiceException, Exception {
        BranchCounter branchCounter = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounters/get/id?branchCounterId="
                + getCounterTicketPayload.getBranchCounterId(),
                BranchCounter.class, xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorDetails());

        // send notification to counter application to move to feedback screen
        BranchCounterConfig branchCounterConfig = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounterconfigs/get/counter?branchCounterId="
                + branchCounter.getCounterId(),
                BranchCounterConfig.class, xyzErrorMessage.COUNTERCONFIG_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.COUNTERCONFIG_NOT_EXISTS.getErrorDetails());

        if (branchCounterConfig != null) {
            if (branchCounterConfig.getFeedBackScreen()) {
                Map<String, String> metaData = new HashMap<String, String>();
                metaData.put("NotificationCode", PushNotification.TICKET_FEEDBACK.getNotificationCode());
                metaData.put("counterNumber", branchCounter.getCounterNumber());

                NotificationSender.sendPushNotificationData(PushNotification.TICKET_FEEDBACK.getNotificationTitle(),
                        PushNotification.TICKET_FEEDBACK.getNotificationDescription(),
                        getCounterTicketPayload.getDeviceToken(), metaData);
            }
        }

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.COUNTER_CONFIG_ADDED.getMessage());
    }

    @Override
    public GenStatusResponse addTvConfig(AddTVConfigPayload addTvConfigPayload)
            throws TaboorQMSServiceException, Exception {

        Branch branch = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branches/get/id?branchId=" + addTvConfigPayload.getBranchId(),
                Branch.class, xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorDetails());

        BranchTVConfig branchTVConfigResponse = RestUtil.getRequestNoCheck(
                dbConnectorMicroserviceURL + "/tab/branchtvconfigs/get/branch?branchId=" + branch.getBranchId(),
                BranchTVConfig.class);

        if (branchTVConfigResponse != null) {
            if (!branchTVConfigResponse.getDeviceToken().equals(addTvConfigPayload.getDeviceToken())) {
                throw new TaboorQMSServiceException(xyzErrorMessage.TVCONFIG_ALREADY_EXISTS.getErrorCode() + "::"
                        + xyzErrorMessage.TVCONFIG_ALREADY_EXISTS.getErrorDetails());
            }
        } else {
            BranchTVConfig branchTVConfig = new BranchTVConfig();
            branchTVConfig.setBranch(branch);
            branchTVConfig.setDeviceToken(addTvConfigPayload.getDeviceToken());

            HttpEntity<BranchTVConfig> entity = new HttpEntity<>(branchTVConfig,
                    HttpHeadersUtils.getApplicationJsonHeader());
            RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/branchtvconfigs/save", entity,
                    BranchTVConfig.class, xyzErrorMessage.TVCONFIG_NOT_CREATED.getErrorCode(),
                    xyzErrorMessage.TVCONFIG_NOT_CREATED.getErrorDetails());
        }

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.TV_CONFIG_ADDED.getMessage());
    }

    @Override
    public GetCountersResponse getAllCounterTickets(GetAllCounterTicketsPayload getAllCounterTicketsPayload)
            throws TaboorQMSServiceException, Exception {

        List<?> branchCounters = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounters/get/branchId?branchId="
                + getAllCounterTicketsPayload.getBranchId(),
                List.class, xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorDetails());

        List<BranchCounter> counterList = new ArrayList<BranchCounter>();
        if (branchCounters != null) {
            counterList = GenericMapper.convertListToObject(branchCounters, new TypeReference<List<BranchCounter>>() {
            });
        }

        if (counterList.isEmpty()) {
            throw new TaboorQMSServiceException(xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorDetails());
        }
        List<GetCountersResponse.GetAllCounters> counters = new ArrayList<GetCountersResponse.GetAllCounters>();
        List<?> ticketListResponse = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                + "/tab/tickets/getTicket/branchId?branchId=" + getAllCounterTicketsPayload.getBranchId(), List.class);
        List<Ticket> ticketList = new ArrayList<Ticket>();
        if (ticketListResponse != null) {
            ticketList = GenericMapper.convertListToObject(ticketListResponse, new TypeReference<List<Ticket>>() {
            });
        }
        for (BranchCounter counter : counterList) {
            BranchCounterAgentMapper counterAgentMapper = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                    + "/tab/branchcounteragent/get/counter?counterId=" + counter.getCounterId(),
                    BranchCounterAgentMapper.class);
            GetCountersResponse.GetAllCounters counterData = new GetCountersResponse().new GetAllCounters();
            counterData.setCounterNumber(counter.getCounterNumber());
            if (counterAgentMapper != null) {
                counterData.setCounterStatus(counterAgentMapper.getAgent().isLoginStatus());
            } else {
                counterData.setCounterStatus(false);
            }

            if (!ticketList.isEmpty()) {
                for (Ticket ticket : ticketList) {
                    if (ticket.getCounterNumber().equals(counter.getCounterNumber())) {
                        counterData.setTicketNumber(ticket.getTicketNumber());
                    } else {
                        counterData.setTicketNumber("");
                    }
                }
            } else {
                counterData.setTicketNumber("");
            }

            counters.add(counterData);
        }

        return new GetCountersResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.COUNTER_TICKETS_FETCHED.getMessage(), counters);
    }

    @Override
    public GenStatusResponse sendTicketChangeTV(TicketChangeCounterNotifPayload payload)
            throws TaboorQMSServiceException, Exception {

        Random random = new Random();
        String ticketN = "";
        Map<String, String> metaData = new HashMap<String, String>();

        if (payload.getCounterFlag()) {
            Map<String, String> metaData2 = new HashMap<String, String>();
            metaData2.put("NotificationCode", PushNotification.CHANGE_AGENT_STATUS.getNotificationCode());
            metaData2.put("CounterNumber", payload.getCounterNumber());
            metaData2.put("CounterStatus", Boolean.TRUE.toString());

            NotificationSender.sendPushNotificationData(PushNotification.CHANGE_AGENT_STATUS.getNotificationTitle(),
                    PushNotification.CHANGE_AGENT_STATUS.getNotificationDescription(), payload.getDeviceToken(),
                    metaData2);
        } else {
            if (payload.getCounterStatus()) {
                ticketN = String.format("%04d", random.nextInt(10000));
                metaData.put("NotificationCode", PushNotification.CHANGE_COUNTER_TICKET.getNotificationCode());
                metaData.put("ticketNumber", "M" + ticketN);
                metaData.put("counterNumber", payload.getCounterNumber());
            } else {
                metaData.put("NotificationCode", PushNotification.CHANGE_COUNTER_TICKET.getNotificationCode());
                metaData.put("ticketNumber", ticketN);
                metaData.put("counterNumber", payload.getCounterNumber());
            }

            NotificationSender.sendPushNotificationData(PushNotification.CHANGE_COUNTER_TICKET.getNotificationTitle(),
                    PushNotification.CHANGE_COUNTER_TICKET.getNotificationDescription(), payload.getDeviceToken(),
                    metaData);
        }

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.TV_CONFIG_ADDED.getMessage());
    }

    @Override
    public GenStatusResponse sendAgentChangeLoginStatus(ChangeCounterStatusPayload counterStatusPayload)
            throws TaboorQMSServiceException, Exception {

        BranchCounter branchCounter = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounters/get/id?branchCounterId="
                + counterStatusPayload.getBranchCounterId(),
                BranchCounter.class, xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorDetails());

        Map<String, String> metaData = new HashMap<String, String>();
        metaData.put("NotificationCode", PushNotification.CHANGE_AGENT_STATUS.getNotificationCode());
        metaData.put("CounterNumber", branchCounter.getCounterNumber().toString());
        metaData.put("CounterStatus", counterStatusPayload.getCounterStatus().toString());

        NotificationSender.sendPushNotificationData(PushNotification.CHANGE_AGENT_STATUS.getNotificationTitle(),
                PushNotification.CHANGE_AGENT_STATUS.getNotificationDescription(),
                counterStatusPayload.getDeviceToken(), metaData);

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.TV_CONFIG_ADDED.getMessage());
    }

    @Override
    public GenStatusResponse sendNewCounterAdded(NewCounterPayload payload)
            throws TaboorQMSServiceException, Exception {

        Map<String, String> metaData = new HashMap<String, String>();
        metaData.put("NotificationCode", PushNotification.NEW_COUNTER_ADDED.getNotificationCode());

        NotificationSender.sendPushNotificationData(PushNotification.NEW_COUNTER_ADDED.getNotificationTitle(),
                PushNotification.NEW_COUNTER_ADDED.getNotificationDescription(), payload.getDeviceToken(), metaData);

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.TV_CONFIG_ADDED.getMessage());
    }

    @Override
    public GenStatusResponse logoutCounter() throws TaboorQMSServiceException, Exception {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();

        RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounterconfigs/delete/deviceToken?deviceToken="
                + session.getDeviceToken(),
                Boolean.class, xyzErrorMessage.COUNTERCONFIG_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.COUNTERCONFIG_NOT_EXISTS.getErrorDetails());

        session.setLogoutTime(OffsetDateTime.now());

        RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/sessions/save",
                new HttpEntity<>(session, HttpHeadersUtils.getApplicationJsonHeader()), Session.class,
                xyzErrorMessage.USER_NOT_LOGOUT.getErrorCode(), xyzErrorMessage.USER_NOT_LOGOUT.getErrorDetails());

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.USER_LOGOUT.getMessage());
    }

    @Override
    public GenStatusResponse logoutTV() throws TaboorQMSServiceException, Exception {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();

        RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchtvconfigs/delete/deviceToken?deviceToken="
                + session.getDeviceToken(),
                Boolean.class, xyzErrorMessage.TVCONFIG_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.TVCONFIG_NOT_EXISTS.getErrorDetails());

        session.setLogoutTime(OffsetDateTime.now());

        RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/sessions/save",
                new HttpEntity<>(session, HttpHeadersUtils.getApplicationJsonHeader()), Session.class,
                xyzErrorMessage.USER_NOT_LOGOUT.getErrorCode(), xyzErrorMessage.USER_NOT_LOGOUT.getErrorDetails());

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.USER_LOGOUT.getMessage());
    }

}
