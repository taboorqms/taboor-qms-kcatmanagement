package com.taboor.qms.kcat.serviceImpl;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.taboor.qms.core.auth.MyUserDetails;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.exception.xyzErrorMessage;
import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.Agent;
import com.taboor.qms.core.model.AgentCounterConfig;
import com.taboor.qms.core.model.AgentTicketCount;
import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.BranchCounter;
import com.taboor.qms.core.model.BranchCounterAgentMapper;
import com.taboor.qms.core.model.BranchCounterConfig;
import com.taboor.qms.core.model.BranchCounterServiceTABMapper;
import com.taboor.qms.core.model.BranchTVConfig;
import com.taboor.qms.core.model.Customer;
import com.taboor.qms.core.model.CustomerSettings;
import com.taboor.qms.core.model.GuestTicket;
import com.taboor.qms.core.model.ServiceRequirement;
import com.taboor.qms.core.model.ServiceTAB;
import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.model.TicketFastpass;
import com.taboor.qms.core.model.TicketServed;
import com.taboor.qms.core.model.TicketUserMapper;
import com.taboor.qms.core.model.User;
import com.taboor.qms.core.payload.CheckAgentTicketCountPayload;
import com.taboor.qms.core.payload.CreateSessionPayload;
import com.taboor.qms.core.payload.GetActivitiesPayload;
import com.taboor.qms.core.payload.GetByIdPayload;
import com.taboor.qms.core.payload.PushUserNotificationPayload;
import com.taboor.qms.core.payload.StepInOutTicketPayload;
import com.taboor.qms.core.response.CreateSessionResponse;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetAgentListResponse;
import com.taboor.qms.core.response.GetAgentListResponse.AgentObject;
import com.taboor.qms.core.response.GetIdListResponse;
import com.taboor.qms.core.response.GetStatisticsResponse;
import com.taboor.qms.core.response.GetStatisticsResponse.ValueObject;
import com.taboor.qms.core.utils.DeviceName;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.HttpHeadersUtils;
import com.taboor.qms.core.utils.NotificationSender;
import com.taboor.qms.core.utils.PrivilegeName;
import com.taboor.qms.core.utils.PushNotification;
import com.taboor.qms.core.utils.RestUtil;
import com.taboor.qms.core.utils.RoleName;
import com.taboor.qms.core.utils.Signature;
import com.taboor.qms.core.utils.TaboorQMSConfigurations;
import com.taboor.qms.core.utils.TicketStatus;
import com.taboor.qms.core.utils.TicketTransferedStatus;
import com.taboor.qms.core.utils.TicketType;
import com.taboor.qms.kcat.model.Queue;
import com.taboor.qms.kcat.model.QueueTicketMapper;
import com.taboor.qms.kcat.model.Ticket;
import com.taboor.qms.kcat.payload.AddAgentCounterConfigPayload;
import com.taboor.qms.kcat.payload.LoginAgentPayload;
import com.taboor.qms.kcat.payload.NewCounterPayload;
import com.taboor.qms.kcat.repository.QueueRepository;
import com.taboor.qms.kcat.repository.QueueTicketMapperRepository;
import com.taboor.qms.kcat.repository.TicketRepository;
import com.taboor.qms.kcat.response.AddAgentCounterConfigResponse;
import com.taboor.qms.kcat.response.GetBranchServicesResponse;
import com.taboor.qms.kcat.response.GetQueueTicketListResponse;
import com.taboor.qms.kcat.response.GetQueueTicketListResponse.TicketObject;
import com.taboor.qms.kcat.response.GetTicketServingResponse;
import com.taboor.qms.kcat.response.GetTransferCountersResponse;
import com.taboor.qms.kcat.response.LoginUserResponse;
import com.taboor.qms.kcat.response.TicketSummaryResponse;
import com.taboor.qms.kcat.service.AgentService;
import java.time.format.TextStyle;
import java.util.Locale;
import org.springframework.web.client.RestClientException;

@Service
public class AgentServiceImpl implements AgentService {

    @Value("${url.microservice.db.connector}")
    private String dbConnectorMicroserviceURL;

    @Value("${url.microservice.user.management}")
    private String userManagementMicroserviceURL;

    @Value("${url.microservice.queue.management}")
    private String queueManagementMicroserviceURL;

    private RestTemplate restTemplate = new RestTemplate();

    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    QueueRepository queueRepository;

    @Autowired
    QueueTicketMapperRepository queueTicketMapperRepository;

    @Override
    public AddAgentCounterConfigResponse addAgentCounterConfig(
            AddAgentCounterConfigPayload addAgentCounterConfigPayload) throws TaboorQMSServiceException, Exception {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();

        BranchCounter branchCounter = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounters/get/id?branchCounterId="
                + addAgentCounterConfigPayload.getBranchCounterId(),
                BranchCounter.class, xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorDetails());

        AgentCounterConfig agentCounterConfigEntity = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                + "/tab/agentcounterconfigs/get/counter?branchCounterId=" + branchCounter.getCounterId(),
                AgentCounterConfig.class);

        CheckAgentTicketCountPayload payload = new CheckAgentTicketCountPayload();
        payload.setServingDate(LocalDate.now());
        payload.setUserId(session.getUser().getUserId());
        payload.setBranchCounterId(branchCounter.getCounterId());

        AgentTicketCount agentTicketCountEntity = RestUtil.postRequestNoCheck(
                dbConnectorMicroserviceURL + "/tab/agentticketcounts/getByDateAndBranchAndUser", payload,
                AgentTicketCount.class);

        if (agentTicketCountEntity == null) {

            AgentTicketCount agentTicketCount = new AgentTicketCount();
            agentTicketCount.setServingAgent(session.getUser());
            agentTicketCount.setServingCounter(branchCounter);
            agentTicketCount.setServingDate(LocalDate.now());
            agentTicketCount.setTotalTicketCancelled(0);
            agentTicketCount.setTotalTicketRecalled(0);
            agentTicketCount.setTotalTicketServed(0);
            agentTicketCount.setTotalTicketTransferred(0);
            agentTicketCount.setTotalSum(0);

            HttpEntity<AgentTicketCount> agentTicketCountDbEntity = new HttpEntity<>(agentTicketCount,
                    HttpHeadersUtils.getApplicationJsonHeader());
            RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/agentticketcounts/save",
                    agentTicketCountDbEntity, AgentTicketCount.class,
                    xyzErrorMessage.AGENTTICKETCOUNT_NOT_CREATED.getErrorCode(),
                    xyzErrorMessage.AGENTTICKETCOUNT_NOT_CREATED.getErrorDetails());
        }

        if (agentCounterConfigEntity != null) {
            AgentCounterConfig agentCounterConfigDbEntity = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                    + "/tab/agentcounterconfigs/get/counterAndToken?branchCounterId=" + branchCounter.getCounterId()
                    + "&deviceToken=" + addAgentCounterConfigPayload.getDeviceToken(), AgentCounterConfig.class);

            if (agentCounterConfigDbEntity == null) {
                throw new TaboorQMSServiceException(xyzErrorMessage.AGENTCONFIG_ALREADY_EXISTS.getErrorCode() + "::"
                        + xyzErrorMessage.AGENTCONFIG_ALREADY_EXISTS.getErrorDetails());
            }

        } else {

            AgentCounterConfig agentCounterConfigDbEntity = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                    + "/tab/agentcounterconfigs/get/token?deviceToken=" + addAgentCounterConfigPayload.getDeviceToken(),
                    AgentCounterConfig.class);

            if (agentCounterConfigDbEntity != null) {
                agentCounterConfigDbEntity.setBranchCounter(branchCounter);
                agentCounterConfigDbEntity.setDeviceToken(addAgentCounterConfigPayload.getDeviceToken());

                HttpEntity<AgentCounterConfig> entity = new HttpEntity<>(agentCounterConfigDbEntity,
                        HttpHeadersUtils.getApplicationJsonHeader());
                RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/agentcounterconfigs/save", entity,
                        AgentCounterConfig.class, xyzErrorMessage.AGENTCOUNTERCONFIG_NOT_CREATED.getErrorCode(),
                        xyzErrorMessage.AGENTCOUNTERCONFIG_NOT_CREATED.getErrorDetails());
            } else {
                AgentCounterConfig agentCounterConfig = new AgentCounterConfig();
                agentCounterConfig.setBranchCounter(branchCounter);
                agentCounterConfig.setDeviceToken(addAgentCounterConfigPayload.getDeviceToken());

                HttpEntity<AgentCounterConfig> entity = new HttpEntity<>(agentCounterConfig,
                        HttpHeadersUtils.getApplicationJsonHeader());
                RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/agentcounterconfigs/save", entity,
                        AgentCounterConfig.class, xyzErrorMessage.AGENTCOUNTERCONFIG_NOT_CREATED.getErrorCode(),
                        xyzErrorMessage.AGENTCOUNTERCONFIG_NOT_CREATED.getErrorDetails());
            }
        }

        // send notification to TV application to change the serving ticket to OPEN
        BranchTVConfig branchTVConfig = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                + "/tab/branchtvconfigs/get/branch?branchId=" + branchCounter.getBranch().getBranchId(),
                BranchTVConfig.class);

        if (branchTVConfig != null) {

            Map<String, String> metaData2 = new HashMap<String, String>();
            metaData2.put("NotificationCode", PushNotification.CHANGE_AGENT_STATUS.getNotificationCode());
            metaData2.put("CounterNumber", branchCounter.getCounterNumber().toString());
            metaData2.put("CounterStatus", Boolean.TRUE.toString());

            NotificationSender.sendPushNotificationData(PushNotification.CHANGE_AGENT_STATUS.getNotificationTitle(),
                    PushNotification.CHANGE_AGENT_STATUS.getNotificationDescription(), branchTVConfig.getDeviceToken(),
                    metaData2);
        }

        // send notification to counter application to change the serving ticket to OPEN
        BranchCounterConfig branchCounterConfig = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                + "/tab/branchcounterconfigs/get/counter?branchCounterId=" + branchCounter.getCounterId(),
                BranchCounterConfig.class);
        if (branchCounterConfig != null) {
            Map<String, String> metaData = new HashMap<String, String>();
            metaData.put("NotificationCode", PushNotification.CHANGE_AGENT_STATUS.getNotificationCode());
            metaData.put("CounterNumber", branchCounter.getCounterNumber().toString());
            metaData.put("CounterStatus", Boolean.TRUE.toString());

            NotificationSender.sendPushNotificationData(PushNotification.CHANGE_AGENT_STATUS.getNotificationTitle(),
                    PushNotification.CHANGE_AGENT_STATUS.getNotificationDescription(),
                    branchCounterConfig.getDeviceToken(), metaData);
        }

        return new AddAgentCounterConfigResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.AGENTCOUNTER_CONFIG_ADDED.getMessage(),
                branchCounter.getBranch().getBranchId().toString(), branchCounter.getCounterNumber(), true);
    }

    @Override
    public LoginUserResponse loginAgent(@Valid LoginAgentPayload loginAgentPayload)
            throws TaboorQMSServiceException, Exception {

        String username = loginAgentPayload.getUserName();
        String password = loginAgentPayload.getPassword();
        String deviceToken = loginAgentPayload.getDeviceToken();
        Integer deviceCode = loginAgentPayload.getDeviceCode();

        Pattern pattern = Pattern.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");
        Matcher mat = pattern.matcher(username);

        List<PrivilegeName> privilegeNames = new ArrayList<PrivilegeName>();

//        Email to be only used by BRANCH_ADMIN and SERVICE_CENTER_ADMIN
        if (mat.matches()) {
            User user = RestUtil.getRequestNoCheck(
                    dbConnectorMicroserviceURL + "/tab/users/get/email?email=" + username, User.class);

            if (user == null) {
                throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_EXISTS.getErrorCode() + "::"
                        + xyzErrorMessage.USER_NOT_EXISTS.getErrorDetails());
            }

            if (!Signature.decrypt(user.getPassword(), TaboorQMSConfigurations.PASSWORD_ENCRYPTION_KEY.getValue())
                    .equals(password)) {
                throw new TaboorQMSServiceException(xyzErrorMessage.USER_PASSWORD_INCORRECT.getErrorCode() + "::"
                        + xyzErrorMessage.USER_PASSWORD_INCORRECT.getErrorDetails());
            }
//          only BRANCH_ADMIN and SERVICE_CENTER_ADMIN can access using EMAIL
            if (deviceCode.equals(DeviceName.AGENT_APP.getCode())
                    && !(user.getRoleName().equals(RoleName.Branch_Admin.name()) || user.getRoleName().equals(RoleName.Service_Center_Admin.name()))) {
                throw new TaboorQMSServiceException(xyzErrorMessage.USERROLE_COUNTER_ACCESS.getErrorCode() + "::"
                        + xyzErrorMessage.USERROLE_COUNTER_ACCESS.getErrorDetails());
            }
//          Removing session check for BRANCH_ADMIN and SERVICE_CENTER_ADMIN
//            if (deviceCode.equals(DeviceName.AGENT_APP.getCode())) {
//                if(!(user.getRoleName().equals(RoleName.Branch_Admin.name()) || user.getRoleName().equals(RoleName.Service_Center_Admin.name()))){
//                    List<?> sessions = RestUtil.getRequestNoCheck(
//                        dbConnectorMicroserviceURL + "/tab/sessions/getByUserEmail?email=" + username, List.class);
//
//				List<Session> sessionList = GenericMapper.convertListToObject(sessions,
//						new TypeReference<List<Session>>() {
//						});
//                    if (!sessions.isEmpty()) {
//                        throw new TaboorQMSServiceException(xyzErrorMessage.SESSION_ALREADY_EXISTS.getErrorCode() + "::"
//                                + xyzErrorMessage.SESSION_ALREADY_EXISTS.getErrorDetails());
//                    }
//                }
//            }

            privilegeNames = GenericMapper.convertListToObject(
                    RestUtil.getRequest(
                            dbConnectorMicroserviceURL + "/tab/userprivilegemapper/getPrivilegeName/user?userId="
                            + user.getUserId(),
                            List.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                            xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails()),
                    new TypeReference<List<PrivilegeName>>() {
            });

            CreateSessionPayload createSessionPayload = new CreateSessionPayload();
            createSessionPayload.setDeviceToken(deviceToken);
            createSessionPayload.setIpAddress(loginAgentPayload.getIpAddress());
            createSessionPayload.setUser(user);

            CreateSessionResponse createSessionResponse = RestUtil.postRequest(
                    userManagementMicroserviceURL + "/session/create", createSessionPayload,
                    CreateSessionResponse.class, xyzErrorMessage.SESSION_NOT_CREATED.getErrorCode(),
                    xyzErrorMessage.SESSION_NOT_CREATED.getErrorDetails());

            AgentCounterConfig agentCounterConfigEntity = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                    + "/tab/agentcounterconfigs/get/token?deviceToken=" + loginAgentPayload.getDeviceToken(),
                    AgentCounterConfig.class);

            CheckAgentTicketCountPayload payload = new CheckAgentTicketCountPayload();
            payload.setServingDate(LocalDate.now());
            payload.setUserId(user.getUserId());
            payload.setBranchCounterId(null);

            AgentTicketCount agentTicketCountEntity = RestUtil.postRequestNoCheck(
                    dbConnectorMicroserviceURL + "/tab/agentticketcounts/getByDateAndUser", payload,
                    AgentTicketCount.class);

            if (agentTicketCountEntity == null) {

                AgentTicketCount agentTicketCount = new AgentTicketCount();
                agentTicketCount.setServingAgent(user);
                agentTicketCount.setServingCounter(null);
                agentTicketCount.setServingDate(LocalDate.now());
                agentTicketCount.setTotalTicketCancelled(0);
                agentTicketCount.setTotalTicketRecalled(0);
                agentTicketCount.setTotalTicketServed(0);
                agentTicketCount.setTotalTicketTransferred(0);
                agentTicketCount.setTotalSum(0);

                HttpEntity<AgentTicketCount> agentTicketCountDbEntity = new HttpEntity<>(agentTicketCount,
                        HttpHeadersUtils.getApplicationJsonHeader());
                RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/agentticketcounts/save",
                        agentTicketCountDbEntity, AgentTicketCount.class,
                        xyzErrorMessage.AGENTTICKETCOUNT_NOT_CREATED.getErrorCode(),
                        xyzErrorMessage.AGENTTICKETCOUNT_NOT_CREATED.getErrorDetails());
            }

            Boolean setup = false;
            if (agentCounterConfigEntity != null) {
                setup = true;
            }

            return new LoginUserResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.USER_LOGIN.getMessage(),
                    createSessionResponse.getSessionToken(), createSessionResponse.getRefreshToken(),
                    user.getRoleName(), setup, privilegeNames);

        } else {
            Agent agent = RestUtil.getRequest(dbConnectorMicroserviceURL + "/tab/agents/get/empNo?agentNo=" + username,
                    Agent.class, xyzErrorMessage.AGENT_NOT_EXISTS.getErrorCode(),
                    xyzErrorMessage.AGENT_NOT_EXISTS.getErrorDetails());

            if (deviceCode.equals(DeviceName.AGENT_APP.getCode())
                    && !agent.getServiceCenterEmployee().getUser().getRoleName().equals(RoleName.Agent.name())) {
                throw new TaboorQMSServiceException(xyzErrorMessage.USERROLE_AGENT_ACCESS.getErrorCode() + "::"
                        + xyzErrorMessage.USERROLE_AGENT_ACCESS.getErrorDetails());
            }

            if (!Signature.decrypt(agent.getServiceCenterEmployee().getUser().getPassword(),
                    TaboorQMSConfigurations.PASSWORD_ENCRYPTION_KEY.getValue()).equals(password)) {
                throw new TaboorQMSServiceException(xyzErrorMessage.USER_PASSWORD_INCORRECT.getErrorCode() + "::"
                        + xyzErrorMessage.USER_PASSWORD_INCORRECT.getErrorDetails());
            }

            List<?> sessions = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                    + "/tab/sessions/getByUserId?userId=" + agent.getServiceCenterEmployee().getUser().getUserId(),
                    List.class);

            if (!sessions.isEmpty()) {
                throw new TaboorQMSServiceException(xyzErrorMessage.SESSION_ALREADY_EXISTS.getErrorCode() + "::"
                        + xyzErrorMessage.SESSION_ALREADY_EXISTS.getErrorDetails());
            }

            AgentCounterConfig agentCounterConfig = RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/agentcounterconfigs/get/token?deviceToken="
                    + loginAgentPayload.getDeviceToken(),
                    AgentCounterConfig.class, xyzErrorMessage.AGENTCOUNTERCONFIG_NOT_EXISTS.getErrorCode(),
                    xyzErrorMessage.AGENTCOUNTERCONFIG_NOT_EXISTS.getErrorDetails());

            BranchCounter branchCounter = agentCounterConfig.getBranchCounter();

            BranchCounterAgentMapper branchCounterAgentMapper = RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/branchcounteragent/get/counter?counterId="
                    + branchCounter.getCounterId(),
                    BranchCounterAgentMapper.class, xyzErrorMessage.BRANCH_COUNTER_AGENT_NOT_EXISTS.getErrorCode(),
                    xyzErrorMessage.BRANCH_COUNTER_AGENT_NOT_EXISTS.getErrorDetails());

            if (!Objects.equals(branchCounterAgentMapper.getAgent().getServiceCenterEmployee().getEmployeeNumber(),
                    agent.getServiceCenterEmployee().getEmployeeNumber())) {
                throw new TaboorQMSServiceException(xyzErrorMessage.AGENT_COUNTER_NOT_ASSIGN.getErrorCode() + "::"
                        + xyzErrorMessage.AGENT_COUNTER_NOT_ASSIGN.getErrorDetails());
            }

            privilegeNames = GenericMapper.convertListToObject(
                    RestUtil.getRequest(
                            dbConnectorMicroserviceURL + "/tab/userprivilegemapper/getPrivilegeName/user?userId="
                            + agent.getServiceCenterEmployee().getUser().getUserId(),
                            List.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                            xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails()),
                    new TypeReference<List<PrivilegeName>>() {
            });

            CreateSessionPayload createSessionPayload = new CreateSessionPayload();
            createSessionPayload.setDeviceToken(deviceToken);
            createSessionPayload.setIpAddress(loginAgentPayload.getIpAddress());
            createSessionPayload.setUser(agent.getServiceCenterEmployee().getUser());

            CreateSessionResponse createSessionResponse = RestUtil.postRequest(
                    userManagementMicroserviceURL + "/session/create", createSessionPayload,
                    CreateSessionResponse.class, xyzErrorMessage.SESSION_NOT_CREATED.getErrorCode(),
                    xyzErrorMessage.SESSION_NOT_CREATED.getErrorDetails());

            agent.setLoginStatus(true);
            agent.getServiceCenterEmployee().getUser().setLastLoginTime(OffsetDateTime.now().withNano(0));

            HttpEntity<Agent> entity = new HttpEntity<>(agent, HttpHeadersUtils.getApplicationJsonHeader());
            RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/agents/save", entity, Agent.class,
                    xyzErrorMessage.AGENT_NOT_UPDATED.getErrorCode(),
                    xyzErrorMessage.AGENT_NOT_UPDATED.getErrorDetails());

            CheckAgentTicketCountPayload payload = new CheckAgentTicketCountPayload();
            payload.setServingDate(LocalDate.now());
            payload.setUserId(agent.getServiceCenterEmployee().getUser().getUserId());
            payload.setBranchCounterId(null);

            AgentTicketCount agentTicketCountEntity = RestUtil.postRequestNoCheck(
                    dbConnectorMicroserviceURL + "/tab/agentticketcounts/getByDateAndUser", payload,
                    AgentTicketCount.class);

            if (agentTicketCountEntity == null) {

                agentTicketCountEntity = new AgentTicketCount();
                agentTicketCountEntity.setServingAgent(agent.getServiceCenterEmployee().getUser());
                agentTicketCountEntity.setServingCounter(branchCounter);
                agentTicketCountEntity.setServingDate(LocalDate.now());
                agentTicketCountEntity.setTotalTicketCancelled(0);
                agentTicketCountEntity.setTotalTicketRecalled(0);
                agentTicketCountEntity.setTotalTicketServed(0);
                agentTicketCountEntity.setTotalTicketTransferred(0);
                agentTicketCountEntity.setTotalSum(0);

                HttpEntity<AgentTicketCount> agentTicketCountDb = new HttpEntity<>(agentTicketCountEntity,
                        HttpHeadersUtils.getApplicationJsonHeader());
                RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/agentticketcounts/save",
                        agentTicketCountDb, AgentTicketCount.class,
                        xyzErrorMessage.AGENTTICKETCOUNT_NOT_CREATED.getErrorCode(),
                        xyzErrorMessage.AGENTTICKETCOUNT_NOT_CREATED.getErrorDetails());
            }

            branchCounter = agentTicketCountEntity.getServingCounter();

            // send notification to counter application to change the counter to OPEN
            BranchCounterConfig branchCounterConfig = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                    + "/tab/branchcounterconfigs/get/counter?branchCounterId=" + branchCounter.getCounterId(),
                    BranchCounterConfig.class);

            if (branchCounterConfig != null) {
                Map<String, String> metaData = new HashMap<String, String>();
                metaData.put("NotificationCode", PushNotification.CHANGE_AGENT_STATUS.getNotificationCode());
                metaData.put("CounterNumber", branchCounterConfig.getBranchCounter().getCounterNumber().toString());
                metaData.put("CounterStatus", Boolean.TRUE.toString());

                NotificationSender.sendPushNotificationData(PushNotification.CHANGE_AGENT_STATUS.getNotificationTitle(),
                        PushNotification.CHANGE_AGENT_STATUS.getNotificationDescription(),
                        branchCounterConfig.getDeviceToken(), metaData);
            }

            // send notification to TV application to change the serving ticket to OPEN
            BranchTVConfig branchTVConfig = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                    + "/tab/branchtvconfigs/get/branch?branchId=" + branchCounter.getBranch().getBranchId(),
                    BranchTVConfig.class);

            if (branchTVConfig != null) {
                Map<String, String> metaData2 = new HashMap<String, String>();
                metaData2.put("NotificationCode", PushNotification.CHANGE_AGENT_STATUS.getNotificationCode());
                metaData2.put("CounterNumber", branchCounter.getCounterNumber().toString());
                metaData2.put("CounterStatus", Boolean.TRUE.toString());

                NotificationSender.sendPushNotificationData(PushNotification.CHANGE_AGENT_STATUS.getNotificationTitle(),
                        PushNotification.CHANGE_AGENT_STATUS.getNotificationDescription(),
                        branchTVConfig.getDeviceToken(), metaData2);
            }

            return new LoginUserResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.USER_LOGIN.getMessage(),
                    createSessionResponse.getSessionToken(), createSessionResponse.getRefreshToken(),
                    agent.getServiceCenterEmployee().getUser().getRoleName(), true, privilegeNames);
        }

    }

    @Override
    public GetBranchServicesResponse getBranchServicesDetails() throws TaboorQMSServiceException, Exception {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();

        if (session.getUser() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.USER_NOT_EXISTS.getErrorDetails());
        }

        AgentCounterConfig agentCounterConfig = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/agentcounterconfigs/get/token?deviceToken="
                + session.getDeviceToken(),
                AgentCounterConfig.class, xyzErrorMessage.AGENTCOUNTERCONFIG_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.AGENTCOUNTERCONFIG_NOT_EXISTS.getErrorDetails());

        BranchCounter branchCounter = agentCounterConfig.getBranchCounter();

        List<GetBranchServicesResponse.ServiceDetails> entityList = new ArrayList<GetBranchServicesResponse.ServiceDetails>();
        List<?> response = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchservicetabmapper/getService/branch?branchId="
                + branchCounter.getBranch().getBranchId(),
                List.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());

        List<ServiceTAB> serviceList = GenericMapper.convertListToObject(response,
                new TypeReference<List<ServiceTAB>>() {
        });

        for (ServiceTAB service : serviceList) {
            GetBranchServicesResponse.ServiceDetails entity = new GetBranchServicesResponse().new ServiceDetails();
            response = RestUtil.getRequest(dbConnectorMicroserviceURL
                    + "/tab/servicecenterservicetabmapper/getRequirements/serviceCenterIdAndServiceId?serviceCenterId="
                    + branchCounter.getBranch().getServiceCenter().getServiceCenterId() + "&serviceId="
                    + service.getServiceId(), List.class, xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                    xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());
            List<ServiceRequirement> serviceRequiredDocumentsList = GenericMapper.convertListToObject(response,
                    new TypeReference<List<ServiceRequirement>>() {
            });
            entity.setServiceName(service.getServiceName());
            entity.setRequirements(serviceRequiredDocumentsList);
            entityList.add(entity);
        }

        if (branchCounter.getBranch().getServiceCenter() != null) {

            return new GetBranchServicesResponse(xyzResponseCode.SUCCESS.getCode(),
                    xyzResponseMessage.BRANCH_SERVICES_FETCHED.getMessage(), branchCounter.getBranch().getBranchName(),
                    branchCounter.getBranch().getCity(), branchCounter.getCounterNumber(), session.getUser().getName(),
                    new String(branchCounter.getBranch().getServiceCenter().getLogoData()), entityList);
        }

        return new GetBranchServicesResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.BRANCH_SERVICES_FETCHED.getMessage(), branchCounter.getBranch().getBranchName(),
                branchCounter.getBranch().getCity(), branchCounter.getCounterNumber(), session.getUser().getName(), "",
                entityList);

    }

    @Override
    public GetTicketServingResponse serveCustomerTicket() throws TaboorQMSServiceException, Exception {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        if (session.getUser() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.USER_NOT_EXISTS.getErrorDetails());
        }

        AgentCounterConfig agentCounterConfig = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/agentcounterconfigs/get/token?deviceToken="
                + session.getDeviceToken(),
                AgentCounterConfig.class, xyzErrorMessage.AGENTCOUNTERCONFIG_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.AGENTCOUNTERCONFIG_NOT_EXISTS.getErrorDetails());

        BranchCounter branchCounter = agentCounterConfig.getBranchCounter();

        List<?> branchCounterServiceTABMapperList = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounterservicetabmapper/get/branchCounter?branchCounterId="
                + branchCounter.getCounterId(),
                List.class, xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorDetails());

        List<BranchCounterServiceTABMapper> counterServiceList = GenericMapper.convertListToObject(
                branchCounterServiceTABMapperList, new TypeReference<List<BranchCounterServiceTABMapper>>() {
        });

        Map<String, String> customerMeta = new HashMap<String, String>();
        customerMeta.put("customerName", "");
        customerMeta.put("customerEmail", "");
        customerMeta.put("customerContact", "");

        HttpHeaders httpHeaders = HttpHeadersUtils.getApplicationJsonHeader();
        List<Ticket> ticketQueue = new ArrayList<Ticket>();
        Queue ticketQueueEn = null;

        for (BranchCounterServiceTABMapper branchCounterServiceTABMapper : counterServiceList) {
            GetQueueTicketListResponse response = getSpecificQueueTickets(branchCounter.getBranch().getBranchId(),
                    branchCounterServiceTABMapper.getServiceTAB().getServiceId());

            ticketQueueEn = response.getQueueInfo();
            System.out.println(" Before loop ticketQueue.size():-->" + response.getTicketList().size());

            if (!response.getTicketList().isEmpty()) {
                for (int i = 0; i < response.getTicketList().size(); i++) {
                    GetQueueTicketListResponse.TicketObject ticketObject = new GetQueueTicketListResponse.TicketObject(
                            response.getTicketList().get(i).getTicket(), response.getTicketList().get(i).getAgent());

                    if (ticketObject.getTicket().getTransferedStatus() == TicketTransferedStatus.TRANSFERED.getStatus()
                            && !ticketObject.getTicket().getTransferedCounterId()
                                    .equals(branchCounter.getCounterId())) {
                    } else if (ticketObject.getTicket().getTicketStatus() == TicketStatus.SERVING.getStatus()
                            && !ticketObject.getTicket().getCounterNumber().equals(branchCounter.getCounterNumber())) {
                    } else if (ticketObject.getTicket().getTransferedStatus() == TicketTransferedStatus.TRANSFERED
                            .getStatus()
                            && ticketObject.getTicket().getTransferedCounterId().equals(branchCounter.getCounterId())) {
                        ticketQueue.add(ticketObject.getTicket());
                    } else {
                        ticketQueue.add(ticketObject.getTicket());
                    }
                }
            }

            System.out.println("After Loop ticketQueue.size():-->" + ticketQueue.size());
        }

        if (ticketQueue.size() == 0) {
            // send notification to TV application to change the serving ticket to OPEN
            BranchTVConfig branchTVConfig = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                    + "/tab/branchtvconfigs/get/branch?branchId=" + branchCounter.getBranch().getBranchId(),
                    BranchTVConfig.class);

            if (branchTVConfig != null) {
                Map<String, String> metaData2 = new HashMap<String, String>();
                metaData2.put("NotificationCode", PushNotification.CHANGE_AGENT_STATUS.getNotificationCode());
                metaData2.put("CounterNumber", branchCounter.getCounterNumber().toString());
                metaData2.put("CounterStatus", Boolean.TRUE.toString());

                NotificationSender.sendPushNotificationData(PushNotification.CHANGE_AGENT_STATUS.getNotificationTitle(),
                        PushNotification.CHANGE_AGENT_STATUS.getNotificationDescription(),
                        branchTVConfig.getDeviceToken(), metaData2);
            }

            // send notification to counter application to change the serving ticket to OPEN
            BranchCounterConfig branchCounterConfig = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                    + "/tab/branchcounterconfigs/get/counter?branchCounterId=" + branchCounter.getCounterId(),
                    BranchCounterConfig.class);
            if (branchCounterConfig != null) {
                Map<String, String> metaData = new HashMap<String, String>();
                metaData.put("NotificationCode", PushNotification.CHANGE_AGENT_STATUS.getNotificationCode());
                metaData.put("CounterNumber", branchCounter.getCounterNumber().toString());
                metaData.put("CounterStatus", Boolean.TRUE.toString());

                NotificationSender.sendPushNotificationData(PushNotification.CHANGE_AGENT_STATUS.getNotificationTitle(),
                        PushNotification.CHANGE_AGENT_STATUS.getNotificationDescription(),
                        branchCounterConfig.getDeviceToken(), metaData);
            }

            if (agentCounterConfig != null) {
                Map<String, String> metaData1 = new HashMap<String, String>();
                metaData1.put("NotificationCode", PushNotification.CHANGE_TICKET_COUNT.getNotificationCode());
                metaData1.put("ticketsWaitingInQueue", String.valueOf(0));

                NotificationSender.sendPushNotificationData(PushNotification.CHANGE_TICKET_COUNT.getNotificationTitle(),
                        PushNotification.CHANGE_TICKET_COUNT.getNotificationDescription(),
                        agentCounterConfig.getDeviceToken(), metaData1);
            }

            throw new TaboorQMSServiceException(xyzErrorMessage.TICKET_IN_QUEUE_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.TICKET_IN_QUEUE_NOT_EXISTS.getErrorDetails());
        }

        Collections.sort(ticketQueue, Comparator.comparing(Ticket::getTransferedStatus)
                .thenComparing(Ticket::getTicketType).thenComparing(Ticket::getPositionTime));

        System.out.println("Out of loop ticketQueue.size():-->" + ticketQueue.size());

        int ticketsWaitingInQueue = ticketQueue.size() - 1;

        ticketQueue.removeIf(t -> t.getTicketType() == TicketType.STEPOUT.getStatus());
//		for (Ticket t : ticketQueue)
//			if (t.getTicketType() == TicketType.STEPOUT.getStatus())
//				ticketQueue.remove(t);

        if (ticketQueue.isEmpty()) {
            throw new TaboorQMSServiceException(xyzErrorMessage.TICKET_IN_QUEUE_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.TICKET_IN_QUEUE_NOT_EXISTS.getErrorDetails());
        }

        Ticket ticket = ticketQueue.get(0);
        List<?> branchCounterList = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounterservicetabmapper/get/branchAndService?branchId="
                + ticket.getBranchId() + "&serviceId=" + ticket.getServiceId(),
                List.class, xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorDetails());

        List<BranchCounter> counterList = GenericMapper.convertListToObject(branchCounterList,
                new TypeReference<List<BranchCounter>>() {
        });

        // get ticket details
        ticket.setCounterNumber(branchCounter.getCounterNumber());
        ticket.setTicketStatus(TicketStatus.SERVING.getStatus());
        ticket.setAgentCallTime(OffsetDateTime.now().withNano(0));

        com.taboor.qms.core.model.Ticket ticketEntity = saveTicket(ticket);
        ticketRepository.save(ticket);

        // send notification to all customer apps in queue to indicate new ticket in
        // queue
        List<?> queueTicketMapperEntityResponse = RestUtil.getRequestNoCheck(
                dbConnectorMicroserviceURL + "/tab/queueticketmapper/get/queueId?queueId=" + ticketQueueEn.getQueueId(),
                List.class);

        List<com.taboor.qms.core.model.QueueTicketMapper> queueTicketMapperEntityList = GenericMapper
                .convertListToObject(queueTicketMapperEntityResponse,
                        new TypeReference<List<com.taboor.qms.core.model.QueueTicketMapper>>() {
                });

        for (com.taboor.qms.core.model.QueueTicketMapper qtm : queueTicketMapperEntityList) {

            TicketUserMapper ticketUserMapperEn = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                    + "/tab/ticketusermapper/get/ticketId?ticketId=" + qtm.getTicket().getTicketId(),
                    TicketUserMapper.class);

            if (ticketUserMapperEn != null) {
                if (ticketUserMapperEn.getUser() != null) {
                    Session userSession = RestUtil.getRequest(
                            dbConnectorMicroserviceURL + "/tab/sessions/getActiveSession/userId?userId="
                            + ticketUserMapperEn.getUser().getUserId(),
                            Session.class, xyzErrorMessage.SESSION_NOT_CREATED.getErrorCode(),
                            xyzErrorMessage.SESSION_NOT_CREATED.getErrorDetails());

                    Map<String, String> metaData2 = new HashMap<String, String>();
                    metaData2.put("NotificationCode", PushNotification.TICKET_GET_QUEUE.getNotificationCode());
                    metaData2.put("ticketNumber", ticketUserMapperEn.getTicket().getTicketNumber());
                    metaData2.put("ticketId", ticketUserMapperEn.getTicket().getTicketId().toString());
                    metaData2.put("branchId", ticketUserMapperEn.getTicket().getBranch().getBranchId().toString());
                    metaData2.put("serviceId", ticketUserMapperEn.getTicket().getService().getServiceId().toString());

                    PushUserNotificationPayload userNotificationPayload = new PushUserNotificationPayload();
                    userNotificationPayload.setTitle(
                            PushNotification.TICKET_GET_QUEUE.getNotificationTitle() + ticket.getTicketNumber());
                    userNotificationPayload
                            .setDescription(PushNotification.TICKET_GET_QUEUE.getNotificationDescription());
                    userNotificationPayload.setMetaData(metaData2);

                    if (userSession.getDeviceToken() != null) {
                        if (!userSession.getDeviceToken().isEmpty()) {
                            NotificationSender.sendPushNotificationData(userNotificationPayload.getTitle(),
                                    userNotificationPayload.getDescription(), userSession.getDeviceToken(),
                                    userNotificationPayload.getMetaData());
                        }
                    }
                }
            } else {
                GuestTicket guestTicket = RestUtil.getRequestNoCheck(
                        dbConnectorMicroserviceURL + "/tab/guesttickets/get/ticket?ticketId=" + ticket.getTicketId(),
                        GuestTicket.class);
                if (guestTicket != null) {
                    Map<String, String> metaData2 = new HashMap<String, String>();
                    metaData2.put("NotificationCode", PushNotification.TICKET_GET_QUEUE.getNotificationCode());
                    metaData2.put("ticketNumber", guestTicket.getTicket().getTicketNumber());
                    metaData2.put("ticketId", guestTicket.getTicket().getTicketId().toString());
                    metaData2.put("branchId", guestTicket.getTicket().getBranch().getBranchId().toString());
                    metaData2.put("serviceId", guestTicket.getTicket().getService().getServiceId().toString());

                    PushUserNotificationPayload userNotificationPayload = new PushUserNotificationPayload();
                    userNotificationPayload.setTitle(
                            PushNotification.TICKET_GET_QUEUE.getNotificationTitle() + ticket.getTicketNumber());
                    userNotificationPayload
                            .setDescription(PushNotification.TICKET_GET_QUEUE.getNotificationDescription());
                    userNotificationPayload.setMetaData(metaData2);

                    if (guestTicket.getDeviceToken() != null) {
                        if (!guestTicket.getDeviceToken().isEmpty()) {
                            NotificationSender.sendPushNotificationData(userNotificationPayload.getTitle(),
                                    userNotificationPayload.getDescription(), guestTicket.getDeviceToken(),
                                    userNotificationPayload.getMetaData());
                        }
                    }
                }
            }
        }

        for (BranchCounter branchCounterEntity : counterList) {
            List<?> branchCounterServiceTABs = RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/branchcounterservicetabmapper/get/branchCounter?branchCounterId="
                    + branchCounterEntity.getCounterId(),
                    List.class, xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorCode(),
                    xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorDetails());

            List<BranchCounterServiceTABMapper> branchCounterServiceList = GenericMapper.convertListToObject(
                    branchCounterServiceTABs, new TypeReference<List<BranchCounterServiceTABMapper>>() {
            });

            int sumTicketsWaitingInQueue = 0;

            for (BranchCounterServiceTABMapper bcst : branchCounterServiceList) {
                int ticketsWaitingInQueueWaiting = getNoOfTicketsInQueue(ticket.getBranchId(),
                        bcst.getServiceTAB().getServiceId(), branchCounterEntity.getCounterId());
                sumTicketsWaitingInQueue += ticketsWaitingInQueueWaiting;
            }

            AgentCounterConfig agentCounterConfigEntity = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                    + "/tab/agentcounterconfigs/get/counter?branchCounterId=" + branchCounterEntity.getCounterId(),
                    AgentCounterConfig.class);

            if (agentCounterConfigEntity != null) {
                Map<String, String> metaData1 = new HashMap<String, String>();
                metaData1.put("NotificationCode", PushNotification.CHANGE_TICKET_COUNT.getNotificationCode());
                metaData1.put("ticketsWaitingInQueue", String.valueOf(sumTicketsWaitingInQueue));

                NotificationSender.sendPushNotificationData(PushNotification.CHANGE_TICKET_COUNT.getNotificationTitle(),
                        PushNotification.CHANGE_TICKET_COUNT.getNotificationDescription(),
                        agentCounterConfigEntity.getDeviceToken(), metaData1);
            }
        }

        BranchCounterAgentMapper branchCounterAgentMapper = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounteragent/get/counter?counterId="
                + branchCounter.getCounterId(),
                BranchCounterAgentMapper.class, xyzErrorMessage.BRANCH_COUNTER_AGENT_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.BRANCH_COUNTER_AGENT_NOT_EXISTS.getErrorDetails());

        TicketServed ticketServedRecord = RestUtil.getRequestNoCheck(
                dbConnectorMicroserviceURL + "/tab/ticketserveds/get/ticket?ticketId=" + ticket.getTicketId(),
                TicketServed.class);

        if (ticketServedRecord != null) {
            ticketServedRecord.setCompletionTime(null);
            ticketServedRecord.setBranchCounter(branchCounter);
            ticketServedRecord.setServedAgent(branchCounterAgentMapper.getAgent());
            ticketServedRecord.setTicket(ticketEntity);
            ticketServedRecord.setAgentCallTime(ticket.getAgentCallTime());
            ticketServedRecord.setTotalTime(null);

            HttpEntity<TicketServed> ticketServedEntity = new HttpEntity<>(ticketServedRecord,
                    HttpHeadersUtils.getApplicationJsonHeader());
            RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/ticketserveds/save", ticketServedEntity,
                    TicketServed.class, xyzErrorMessage.TICKET_SERVED_NOT_CREATED.getErrorCode(),
                    xyzErrorMessage.TICKET_SERVED_NOT_CREATED.getErrorDetails());

        } else {
            TicketServed ticketServed = new TicketServed();
            ticketServed.setCompletionTime(null);
            ticketServed.setBranchCounter(branchCounter);
            ticketServed.setServedAgent(branchCounterAgentMapper.getAgent());
            ticketServed.setTicket(ticketEntity);
            ticketServed.setAgentCallTime(ticket.getAgentCallTime());
            ticketServed.setTotalTime(null);

            HttpEntity<TicketServed> ticketServedEntity = new HttpEntity<>(ticketServed,
                    HttpHeadersUtils.getApplicationJsonHeader());
            RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/ticketserveds/save", ticketServedEntity,
                    TicketServed.class, xyzErrorMessage.TICKET_SERVED_NOT_CREATED.getErrorCode(),
                    xyzErrorMessage.TICKET_SERVED_NOT_CREATED.getErrorDetails());

        }

        TicketUserMapper ticketUserMapper = RestUtil.getRequestNoCheck(
                dbConnectorMicroserviceURL + "/tab/ticketusermapper/get/ticketId?ticketId=" + ticket.getTicketId(),
                TicketUserMapper.class);

        if (ticketUserMapper != null) {
            if (ticketUserMapper.getUser() != null) {

                System.out.println(
                        "In Ticket User Mapper Now ------------>>>>>> ID:" + ticketUserMapper.getUser().getUserId());

                Session userSession = RestUtil.getRequest(
                        dbConnectorMicroserviceURL + "/tab/sessions/getActiveSession/userId?userId="
                        + ticketUserMapper.getUser().getUserId(),
                        Session.class, xyzErrorMessage.SESSION_NOT_CREATED.getErrorCode(),
                        xyzErrorMessage.SESSION_NOT_CREATED.getErrorDetails());

                // send notification to customer application to move to counter to serve ticket
                Map<String, String> metaData = new HashMap<String, String>();
                metaData.put("NotificationCode", PushNotification.TICKET_SERVING_TURN.getNotificationCode());
                metaData.put("ticketNumber", ticket.getTicketNumber());
                metaData.put("ticketId", ticket.getTicketId().toString());
                metaData.put("branchId", ticket.getBranchId().toString());
                metaData.put("serviceId", ticket.getServiceId().toString());
                metaData.put("branchName", getBranch(ticket.getBranchId()).getBranchName());
                metaData.put("branchAddress", getBranch(ticket.getBranchId()).getAddress());
                metaData.put("serviceName", getServiceTAB(ticket.getServiceId()).getServiceName());

                PushUserNotificationPayload userNotificationPayload = new PushUserNotificationPayload();
                userNotificationPayload.setTitle(
                        PushNotification.TICKET_SERVING_TURN.getNotificationTitle() + ticket.getTicketNumber());
                userNotificationPayload.setDescription(PushNotification.TICKET_SERVING_TURN.getNotificationDescription()
                        + branchCounter.getCounterNumber());
                userNotificationPayload.setMetaData(metaData);

                String uri = userManagementMicroserviceURL + "/notification/push";
                httpHeaders = HttpHeadersUtils.getApplicationJsonHeader();
                httpHeaders.setBearerAuth(session.getSessionToken());
                HttpEntity<PushUserNotificationPayload> userNotificationEntity = new HttpEntity<>(
                        userNotificationPayload, httpHeaders);
                ResponseEntity<GenStatusResponse> pushNotificationResponse = restTemplate.postForEntity(uri,
                        userNotificationEntity, GenStatusResponse.class);

                if (!pushNotificationResponse.getStatusCode().equals(HttpStatus.OK) || pushNotificationResponse
                        .getBody().getApplicationStatusCode() == xyzResponseCode.ERROR.getCode()) {
                    throw new TaboorQMSServiceException(
                            444 + "::" + pushNotificationResponse.getBody().getApplicationStatusResponse());
                }

                if (userSession.getDeviceToken() != null) {
                    if (!userSession.getDeviceToken().isEmpty()) {

                        System.out.println("Sending User Notification for counter to going ---->>>>>>"
                                + userNotificationPayload.getTitle() + "  " + userNotificationPayload.getDescription()
                                + "  " + userSession.getDeviceToken());

                        NotificationSender.sendPushNotificationData(userNotificationPayload.getTitle(),
                                userNotificationPayload.getDescription(), userSession.getDeviceToken(), metaData);
                    }
                }
            }
        } else {
            GuestTicket guestTicket = RestUtil.getRequestNoCheck(
                    dbConnectorMicroserviceURL + "/tab/guesttickets/get/ticket?ticketId=" + ticket.getTicketId(),
                    GuestTicket.class);

            if (guestTicket != null) {
                // send notification to customer application to move to counter to serve ticket
                Map<String, String> metaData = new HashMap<String, String>();
                metaData.put("NotificationCode", PushNotification.TICKET_SERVING_TURN.getNotificationCode());
                metaData.put("ticketNumber", ticket.getTicketNumber());
                metaData.put("ticketId", ticket.getTicketId().toString());
                metaData.put("branchId", ticket.getBranchId().toString());
                metaData.put("serviceId", ticket.getServiceId().toString());
                metaData.put("branchName", getBranch(ticket.getBranchId()).getBranchName());
                metaData.put("branchAddress", getBranch(ticket.getBranchId()).getAddress());
                metaData.put("serviceName", getServiceTAB(ticket.getServiceId()).getServiceName());

                PushUserNotificationPayload userNotificationPayload = new PushUserNotificationPayload();
                userNotificationPayload.setTitle(
                        PushNotification.TICKET_SERVING_TURN.getNotificationTitle() + ticket.getTicketNumber());
                userNotificationPayload.setDescription(PushNotification.TICKET_SERVING_TURN.getNotificationDescription()
                        + branchCounter.getCounterNumber());
                userNotificationPayload.setMetaData(metaData);

                String uri = userManagementMicroserviceURL + "/notification/push";
                httpHeaders = HttpHeadersUtils.getApplicationJsonHeader();
                httpHeaders.setBearerAuth(session.getSessionToken());
                HttpEntity<PushUserNotificationPayload> userNotificationEntity = new HttpEntity<>(
                        userNotificationPayload, httpHeaders);
                ResponseEntity<GenStatusResponse> pushNotificationResponse = restTemplate.postForEntity(uri,
                        userNotificationEntity, GenStatusResponse.class);

                if (!pushNotificationResponse.getStatusCode().equals(HttpStatus.OK) || pushNotificationResponse
                        .getBody().getApplicationStatusCode() == xyzResponseCode.ERROR.getCode()) {
                    throw new TaboorQMSServiceException(
                            444 + "::" + pushNotificationResponse.getBody().getApplicationStatusResponse());
                }

                if (guestTicket.getDeviceToken() != null) {
                    if (!guestTicket.getDeviceToken().isEmpty()) {

                        NotificationSender.sendPushNotificationData(userNotificationPayload.getTitle(),
                                userNotificationPayload.getDescription(), guestTicket.getDeviceToken(), metaData);
                    }
                }
            }
        }

        // send notification to counter application to change the serving ticket
        BranchCounterConfig branchCounterConfig = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                + "/tab/branchcounterconfigs/get/counter?branchCounterId=" + branchCounter.getCounterId(),
                BranchCounterConfig.class);

        if (branchCounterConfig != null) {
            Map<String, String> metaData1 = new HashMap<String, String>();
            metaData1.put("NotificationCode", PushNotification.TICKET_SERVING_COUNTER.getNotificationCode());
            metaData1.put("ticketNumber", ticket.getTicketNumber());
            metaData1.put("counterNumber", branchCounter.getCounterNumber());

            NotificationSender.sendPushNotificationData(PushNotification.TICKET_SERVING_COUNTER.getNotificationTitle(),
                    PushNotification.TICKET_SERVING_COUNTER.getNotificationDescription(),
                    branchCounterConfig.getDeviceToken(), metaData1);
        }

        // send notification to TV application to change the serving ticket of counter
        BranchTVConfig branchTVConfig = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                + "/tab/branchtvconfigs/get/branch?branchId=" + branchCounter.getBranch().getBranchId(),
                BranchTVConfig.class);

        if (branchTVConfig != null) {
            Map<String, String> metaData2 = new HashMap<String, String>();
            metaData2.put("NotificationCode", PushNotification.CHANGE_COUNTER_TICKET.getNotificationCode());
            metaData2.put("ticketNumber", ticket.getTicketNumber());
            metaData2.put("counterNumber", branchCounter.getCounterNumber());

            NotificationSender.sendPushNotificationData(PushNotification.CHANGE_COUNTER_TICKET.getNotificationTitle(),
                    PushNotification.CHANGE_COUNTER_TICKET.getNotificationDescription(),
                    branchTVConfig.getDeviceToken(), metaData2);
        }

        if (ticketUserMapper != null) {
            if (ticketUserMapper.getUser() != null) {

                Customer customer = RestUtil.getRequest(
                        dbConnectorMicroserviceURL + "/tab/customers/get/userEmail?email="
                        + ticketUserMapper.getUser().getEmail(),
                        Customer.class, xyzErrorMessage.CUSTOMER_NOT_EXISTS.getErrorCode(),
                        xyzErrorMessage.CUSTOMER_NOT_EXISTS.getErrorDetails());

                CustomerSettings customerSettings = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                        + "/tab/customersettings/getByCustomerId?customerId=" + customer.getCustomerId(),
                        CustomerSettings.class);

                if (customerSettings != null) {
                    if (customerSettings.getPrivacyName()) {
                        customerMeta.replace("customerName", ticketUserMapper.getUser().getName());
                    }
                    if (customerSettings.getPrivacyEmail()) {
                        customerMeta.replace("customerEmail", ticketUserMapper.getUser().getEmail());
                    }
                    if (customerSettings.getPrivacyPhoneNumber()) {
                        customerMeta.replace("customerContact", ticketUserMapper.getUser().getPhoneNumber());
                    }
                }
            }
        }

        return new GetTicketServingResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.TICKET_SERVING_SUCCESS.getMessage(), ticket, ticketsWaitingInQueue,
                customerMeta.get("customerName"), customerMeta.get("customerEmail"),
                customerMeta.get("customerContact"));
    }

    @Override
    public GenStatusResponse cancelCustomerTicket(@Valid StepInOutTicketPayload cancelTicketPayload)
            throws TaboorQMSServiceException, Exception {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        if (session.getUser() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.USER_NOT_EXISTS.getErrorDetails());
        }

        Optional<Ticket> ticketOp = ticketRepository.findById(cancelTicketPayload.getTicketId());
        Ticket ticket = ticketOp.get();

        if (ticket.getTicketType() == TicketType.FASTPASS.getStatus()) {
            GetByIdPayload payload = new GetByIdPayload();
            payload.setId(ticket.getTicketId());

            HttpHeaders httpHeaders = HttpHeadersUtils.getApplicationJsonHeader();
            httpHeaders.setBearerAuth(session.getSessionToken());
            HttpEntity<GetByIdPayload> payloadEntity = new HttpEntity<>(payload, httpHeaders);

            ResponseEntity<TicketFastpass> fastPassResponse = restTemplate.postForEntity(
                    queueManagementMicroserviceURL + "/ticketFastpass/fastpass/cancel", payloadEntity,
                    TicketFastpass.class);

            if (!fastPassResponse.getStatusCode().equals(HttpStatus.OK) || fastPassResponse.getBody() == null) {
                throw new TaboorQMSServiceException(xyzErrorMessage.TICKETFASTPASS_NOT_UPDATED.getErrorCode() + "::"
                        + xyzErrorMessage.TICKETFASTPASS_NOT_UPDATED.getErrorDetails());
            }

        }

        ticket.setTicketType(TicketType.CANCELED.getStatus());

        ticketRepository.save(ticket);
        saveTicket(ticket);

        String uri = dbConnectorMicroserviceURL + "/tab/queueticketmapper/delete/ticketId?ticketId="
                + ticket.getTicketId();
        ResponseEntity<Integer> deleteQueueTicketResponse = restTemplate.getForEntity(uri, Integer.class);
        if (!deleteQueueTicketResponse.getStatusCode().equals(HttpStatus.OK)
                || deleteQueueTicketResponse.getBody() == 0) {
            throw new TaboorQMSServiceException(xyzErrorMessage.TICKET_IN_QUEUE_NOT_DELETED.getErrorCode() + "::"
                    + xyzErrorMessage.TICKET_IN_QUEUE_NOT_DELETED.getErrorDetails());
        }

        queueTicketMapperRepository.deleteByTicketId(ticket.getTicketId());

        AgentCounterConfig agentCounterConfigEntity = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                + "/tab/agentcounterconfigs/get/token?deviceToken=" + session.getDeviceToken(),
                AgentCounterConfig.class);

        CheckAgentTicketCountPayload payload1 = new CheckAgentTicketCountPayload();
        payload1.setServingDate(LocalDate.now());
        payload1.setUserId(session.getUser().getUserId());
        payload1.setBranchCounterId(agentCounterConfigEntity.getBranchCounter().getCounterId());

        AgentTicketCount agentTicketCount = RestUtil.postRequestNoCheck(
                dbConnectorMicroserviceURL + "/tab/agentticketcounts/getByDateAndBranchAndUser", payload1,
                AgentTicketCount.class);

        agentTicketCount.setTotalTicketCancelled(agentTicketCount.getTotalTicketCancelled() + 1);

        HttpEntity<AgentTicketCount> agentTicketCountEntity = new HttpEntity<>(agentTicketCount,
                HttpHeadersUtils.getApplicationJsonHeader());
        RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/agentticketcounts/save", agentTicketCountEntity,
                AgentTicketCount.class, xyzErrorMessage.AGENTTICKETCOUNT_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.AGENTTICKETCOUNT_NOT_CREATED.getErrorDetails());

        TicketUserMapper ticketUserMapper = RestUtil.getRequestNoCheck(
                dbConnectorMicroserviceURL + "/tab/ticketusermapper/get/ticketId?ticketId=" + ticket.getTicketId(),
                TicketUserMapper.class);

        if (ticketUserMapper != null) {
            if (ticketUserMapper.getUser() != null) {
                Session userSession = RestUtil.getRequest(
                        dbConnectorMicroserviceURL + "/tab/sessions/getActiveSession/userId?userId="
                        + ticketUserMapper.getUser().getUserId(),
                        Session.class, xyzErrorMessage.SESSION_NOT_CREATED.getErrorCode(),
                        xyzErrorMessage.SESSION_NOT_CREATED.getErrorDetails());

                Map<String, String> metaData = new HashMap<String, String>();
                metaData.put("NotificationCode", PushNotification.TICKET_CANCEL_1.getNotificationCode());
                metaData.put("ticketNumber", ticket.getTicketNumber());
                metaData.put("ticketId", ticket.getTicketId().toString());
                metaData.put("branchId", ticket.getBranchId().toString());
                metaData.put("serviceId", ticket.getServiceId().toString());
                metaData.put("branchName", getBranch(ticket.getBranchId()).getBranchName());
                metaData.put("branchAddress", getBranch(ticket.getBranchId()).getAddress());
                metaData.put("serviceName", getServiceTAB(ticket.getServiceId()).getServiceName());

                PushUserNotificationPayload userNotificationPayload = new PushUserNotificationPayload();
                if (ticket.getTicketType() == TicketType.FASTPASS.getStatus()) {
                    userNotificationPayload
                            .setTitle(PushNotification.TICKET_FASTPASS_CANCEL_1.getNotificationTitle() + ticket.getTicketNumber());
                    userNotificationPayload.setDescription(PushNotification.TICKET_FASTPASS_CANCEL_1.getNotificationDescription() + ticket.getTicketNumber() + PushNotification.TICKET_FASTPASS_CANCEL_2.getNotificationDescription());

                } else {
                    userNotificationPayload
                            .setTitle(PushNotification.TICKET_CANCEL_1.getNotificationTitle() + ticket.getTicketNumber());
                    userNotificationPayload.setDescription(PushNotification.TICKET_CANCEL_1.getNotificationDescription() + ticket.getTicketNumber() + PushNotification.TICKET_CANCEL_2.getNotificationDescription());

                }
                userNotificationPayload.setMetaData(metaData);

                uri = userManagementMicroserviceURL + "/notification/push";
                HttpHeaders httpHeaders = HttpHeadersUtils.getApplicationJsonHeader();
                httpHeaders.setBearerAuth(session.getSessionToken());
                HttpEntity<PushUserNotificationPayload> userNotificationEntity = new HttpEntity<>(
                        userNotificationPayload, httpHeaders);
                ResponseEntity<GenStatusResponse> pushNotificationResponse = restTemplate.postForEntity(uri,
                        userNotificationEntity, GenStatusResponse.class);

                if (!pushNotificationResponse.getStatusCode().equals(HttpStatus.OK) || pushNotificationResponse
                        .getBody().getApplicationStatusCode() == xyzResponseCode.ERROR.getCode()) {
                    throw new TaboorQMSServiceException(
                            444 + "::" + pushNotificationResponse.getBody().getApplicationStatusResponse());
                }

                if (userSession.getDeviceToken() != null) {
                    if (!userSession.getDeviceToken().isEmpty()) {
                        NotificationSender.sendPushNotificationData(userNotificationPayload.getTitle(),
                                userNotificationPayload.getDescription(), userSession.getDeviceToken(), metaData);
                    }
                }

            }
        } else {
            GuestTicket guestTicket = RestUtil.getRequestNoCheck(
                    dbConnectorMicroserviceURL + "/tab/guesttickets/get/ticket?ticketId=" + ticket.getTicketId(),
                    GuestTicket.class);

            if (guestTicket != null) {
                Map<String, String> metaData = new HashMap<String, String>();
                metaData.put("NotificationCode", PushNotification.TICKET_CANCEL_1.getNotificationCode());
                metaData.put("ticketNumber", ticket.getTicketNumber());
                metaData.put("ticketId", ticket.getTicketId().toString());
                metaData.put("branchId", ticket.getBranchId().toString());
                metaData.put("serviceId", ticket.getServiceId().toString());
                metaData.put("branchName", getBranch(ticket.getBranchId()).getBranchName());
                metaData.put("branchAddress", getBranch(ticket.getBranchId()).getAddress());
                metaData.put("serviceName", getServiceTAB(ticket.getServiceId()).getServiceName());

                PushUserNotificationPayload userNotificationPayload = new PushUserNotificationPayload();
                if (ticket.getTicketType() == TicketType.FASTPASS.getStatus()) {
                    userNotificationPayload
                            .setTitle(PushNotification.TICKET_FASTPASS_CANCEL_1.getNotificationTitle() + ticket.getTicketNumber());
                    userNotificationPayload.setDescription(PushNotification.TICKET_FASTPASS_CANCEL_1.getNotificationDescription() + ticket.getTicketNumber() + PushNotification.TICKET_FASTPASS_CANCEL_2.getNotificationDescription());

                } else {
                    userNotificationPayload
                            .setTitle(PushNotification.TICKET_CANCEL_1.getNotificationTitle() + ticket.getTicketNumber());
                    userNotificationPayload.setDescription(PushNotification.TICKET_CANCEL_1.getNotificationDescription() + ticket.getTicketNumber() + PushNotification.TICKET_CANCEL_2.getNotificationDescription());

                }
                userNotificationPayload.setMetaData(metaData);

                uri = userManagementMicroserviceURL + "/notification/push";
                HttpHeaders httpHeaders = HttpHeadersUtils.getApplicationJsonHeader();
                httpHeaders.setBearerAuth(session.getSessionToken());
                HttpEntity<PushUserNotificationPayload> userNotificationEntity = new HttpEntity<>(
                        userNotificationPayload, httpHeaders);
                ResponseEntity<GenStatusResponse> pushNotificationResponse = restTemplate.postForEntity(uri,
                        userNotificationEntity, GenStatusResponse.class);

                if (!pushNotificationResponse.getStatusCode().equals(HttpStatus.OK) || pushNotificationResponse
                        .getBody().getApplicationStatusCode() == xyzResponseCode.ERROR.getCode()) {
                    throw new TaboorQMSServiceException(
                            444 + "::" + pushNotificationResponse.getBody().getApplicationStatusResponse());
                }

                if (guestTicket.getDeviceToken() != null) {
                    if (!guestTicket.getDeviceToken().isEmpty()) {
                        NotificationSender.sendPushNotificationData(userNotificationPayload.getTitle(),
                                userNotificationPayload.getDescription(), guestTicket.getDeviceToken(), metaData);
                    }
                }
            }
        }

        // send notification to counters agent application to change no of tickets in
        // queue
        List<?> branchCounterList = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounterservicetabmapper/get/branchAndService?branchId="
                + ticket.getBranchId() + "&serviceId=" + ticket.getServiceId(),
                List.class, xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorDetails());

        List<BranchCounter> counterList = GenericMapper.convertListToObject(branchCounterList,
                new TypeReference<List<BranchCounter>>() {
        });

        for (BranchCounter branchCounterEntity : counterList) {
            List<?> branchCounterServiceTABs = RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/branchcounterservicetabmapper/get/branchCounter?branchCounterId="
                    + branchCounterEntity.getCounterId(),
                    List.class, xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorCode(),
                    xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorDetails());

            List<BranchCounterServiceTABMapper> branchCounterServiceList = GenericMapper.convertListToObject(
                    branchCounterServiceTABs, new TypeReference<List<BranchCounterServiceTABMapper>>() {
            });

            int sumTicketsWaitingInQueue = 0;

            for (BranchCounterServiceTABMapper bcst : branchCounterServiceList) {
                int ticketsWaitingInQueueWaiting = getNoOfTicketsInQueue(ticket.getBranchId(),
                        bcst.getServiceTAB().getServiceId(), branchCounterEntity.getCounterId());
                sumTicketsWaitingInQueue += ticketsWaitingInQueueWaiting;
            }

            AgentCounterConfig agentCounterConfig = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                    + "/tab/agentcounterconfigs/get/counter?branchCounterId=" + branchCounterEntity.getCounterId(),
                    AgentCounterConfig.class);

            if (agentCounterConfig != null) {
                Map<String, String> metaData1 = new HashMap<String, String>();
                metaData1.put("NotificationCode", PushNotification.CHANGE_TICKET_COUNT.getNotificationCode());
                metaData1.put("ticketsWaitingInQueue", String.valueOf(sumTicketsWaitingInQueue));

                NotificationSender.sendPushNotificationData(PushNotification.CHANGE_TICKET_COUNT.getNotificationTitle(),
                        PushNotification.CHANGE_TICKET_COUNT.getNotificationDescription(),
                        agentCounterConfig.getDeviceToken(), metaData1);
            }
        }

        // send notification to counter application to change the serving ticket to OPEN
        BranchCounterConfig branchCounterConfig = RestUtil
                .getRequestNoCheck(
                        dbConnectorMicroserviceURL + "/tab/branchcounterconfigs/get/counter?branchCounterId="
                        + agentCounterConfigEntity.getBranchCounter().getCounterId(),
                        BranchCounterConfig.class);
        if (branchCounterConfig != null) {
            Map<String, String> metaData = new HashMap<String, String>();
            metaData.put("NotificationCode", PushNotification.CHANGE_AGENT_STATUS.getNotificationCode());
            metaData.put("CounterNumber", branchCounterConfig.getBranchCounter().getCounterNumber().toString());
            metaData.put("CounterStatus", Boolean.TRUE.toString());

            NotificationSender.sendPushNotificationData(PushNotification.CHANGE_AGENT_STATUS.getNotificationTitle(),
                    PushNotification.CHANGE_AGENT_STATUS.getNotificationDescription(),
                    branchCounterConfig.getDeviceToken(), metaData);
        }

        // send notification to TV application to change the serving ticket to OPEN
        BranchTVConfig branchTVConfig = RestUtil
                .getRequestNoCheck(
                        dbConnectorMicroserviceURL + "/tab/branchtvconfigs/get/branch?branchId="
                        + agentCounterConfigEntity.getBranchCounter().getBranch().getBranchId(),
                        BranchTVConfig.class);

        if (branchTVConfig != null) {
            Map<String, String> metaData2 = new HashMap<String, String>();
            metaData2.put("NotificationCode", PushNotification.CHANGE_AGENT_STATUS.getNotificationCode());
            metaData2.put("CounterNumber", agentCounterConfigEntity.getBranchCounter().getCounterNumber().toString());
            metaData2.put("CounterStatus", Boolean.TRUE.toString());

            NotificationSender.sendPushNotificationData(PushNotification.CHANGE_AGENT_STATUS.getNotificationTitle(),
                    PushNotification.CHANGE_AGENT_STATUS.getNotificationDescription(), branchTVConfig.getDeviceToken(),
                    metaData2);
        }

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.TICKET_CANCELLED.getMessage());
    }

    @Override
    public GetTicketServingResponse callNextCustomerTicket(@Valid StepInOutTicketPayload nextTicketPayload)
            throws TaboorQMSServiceException, Exception {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();

        if (nextTicketPayload.getTicketId() != null) {

            Optional<Ticket> ticketOp = ticketRepository.findById(nextTicketPayload.getTicketId());
            Ticket ticket = ticketOp.get();

            // get ticket details
            ticket.setTicketStatus(TicketStatus.SERVED.getStatus());
            com.taboor.qms.core.model.Ticket ticketEntity = saveTicket(ticket);
            ticketRepository.save(ticket);

            AgentCounterConfig agentCounterConfigEntity = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                    + "/tab/agentcounterconfigs/get/token?deviceToken=" + session.getDeviceToken(),
                    AgentCounterConfig.class);

            CheckAgentTicketCountPayload payload1 = new CheckAgentTicketCountPayload();
            payload1.setServingDate(LocalDate.now());
            payload1.setUserId(session.getUser().getUserId());
            payload1.setBranchCounterId(agentCounterConfigEntity.getBranchCounter().getCounterId());

            AgentTicketCount agentTicketCount = RestUtil.postRequestNoCheck(
                    dbConnectorMicroserviceURL + "/tab/agentticketcounts/getByDateAndBranchAndUser", payload1,
                    AgentTicketCount.class);

            if (agentTicketCount != null) {
                agentTicketCount.setTotalTicketServed(agentTicketCount.getTotalTicketServed() + 1);

                HttpEntity<AgentTicketCount> agentTicketCountEntity = new HttpEntity<>(agentTicketCount,
                        HttpHeadersUtils.getApplicationJsonHeader());
                RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/agentticketcounts/save",
                        agentTicketCountEntity, AgentTicketCount.class,
                        xyzErrorMessage.AGENTTICKETCOUNT_NOT_CREATED.getErrorCode(),
                        xyzErrorMessage.AGENTTICKETCOUNT_NOT_CREATED.getErrorDetails());
            }

            // delete ticket from the queue
            String uri = dbConnectorMicroserviceURL + "/tab/queueticketmapper/delete/ticketId?ticketId="
                    + ticket.getTicketId();
            ResponseEntity<Integer> deleteQueueTicketResponse = restTemplate.getForEntity(uri, Integer.class);
            if (!deleteQueueTicketResponse.getStatusCode().equals(HttpStatus.OK)
                    || deleteQueueTicketResponse.getBody() == 0) {
                throw new TaboorQMSServiceException(xyzErrorMessage.TICKET_IN_QUEUE_NOT_DELETED.getErrorCode() + "::"
                        + xyzErrorMessage.TICKET_IN_QUEUE_NOT_DELETED.getErrorDetails());
            }

            queueTicketMapperRepository.deleteByTicketId(ticket.getTicketId());

            // get ticket served
            TicketServed ticketServed = RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/ticketserveds/get/ticket?ticketId=" + ticket.getTicketId(),
                    TicketServed.class, xyzErrorMessage.BRANCH_COUNTER_AGENT_NOT_EXISTS.getErrorCode(),
                    xyzErrorMessage.BRANCH_COUNTER_AGENT_NOT_EXISTS.getErrorDetails());

            ticketServed.setCompletionTime(OffsetDateTime.now().withNano(0));
            ticketServed.setTicket(ticketEntity);
            ticketServed.setAgentCallTime(ticket.getAgentCallTime());
            ticketServed.setTotalTime(LocalTime.of(0, 38));

            HttpEntity<TicketServed> ticketServedEntity = new HttpEntity<>(ticketServed,
                    HttpHeadersUtils.getApplicationJsonHeader());
            RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/ticketserveds/save", ticketServedEntity,
                    TicketServed.class, xyzErrorMessage.TICKET_SERVED_NOT_CREATED.getErrorCode(),
                    xyzErrorMessage.TICKET_SERVED_NOT_CREATED.getErrorDetails());

            RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL + "/tab/ticketserveds/updateTimings?ticketId="
                    + ticketEntity.getTicketId(), Boolean.class);

            BranchCounterAgentMapper branchCounterAgentMapper = RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/branchcounteragent/get/counter?counterId="
                    + ticketServed.getBranchCounter().getCounterId(),
                    BranchCounterAgentMapper.class, xyzErrorMessage.BRANCH_COUNTER_AGENT_NOT_EXISTS.getErrorCode(),
                    xyzErrorMessage.BRANCH_COUNTER_AGENT_NOT_EXISTS.getErrorDetails());

            branchCounterAgentMapper.getAgent()
                    .setTotalTicketServed(branchCounterAgentMapper.getAgent().getTotalTicketServed() + 1);

            HttpEntity<Agent> entity1 = new HttpEntity<>(branchCounterAgentMapper.getAgent(),
                    HttpHeadersUtils.getApplicationJsonHeader());
            RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/agents/save", entity1, Agent.class,
                    xyzErrorMessage.AGENT_NOT_UPDATED.getErrorCode(),
                    xyzErrorMessage.AGENT_NOT_UPDATED.getErrorDetails());

            TicketUserMapper ticketUserMapper = RestUtil.getRequestNoCheck(
                    dbConnectorMicroserviceURL + "/tab/ticketusermapper/get/ticketId?ticketId=" + ticket.getTicketId(),
                    TicketUserMapper.class);

            if (ticketUserMapper != null) {
                if (ticketUserMapper.getUser() != null) {

                    System.out.println("In Ticket User Mapper Now ------------>>>>>> ID:"
                            + ticketUserMapper.getUser().getUserId());

                    Session userSession = RestUtil.getRequest(
                            dbConnectorMicroserviceURL + "/tab/sessions/getActiveSession/userId?userId="
                            + ticketUserMapper.getUser().getUserId(),
                            Session.class, xyzErrorMessage.SESSION_NOT_CREATED.getErrorCode(),
                            xyzErrorMessage.SESSION_NOT_CREATED.getErrorDetails());

                    Map<String, String> metaData = new HashMap<String, String>();
                    metaData.put("NotificationCode", PushNotification.TICKET_GET_TIMELINE.getNotificationCode());
                    metaData.put("ticketid", ticketUserMapper.getTicket().getTicketId().toString());
                    metaData.put("ticketNumber", ticketUserMapper.getTicket().getTicketNumber());
                    metaData.put("ticketType", Integer.toString(ticketUserMapper.getTicket().getTicketType()));

                    if (userSession.getDeviceToken() != null) {
                        if (!userSession.getDeviceToken().isEmpty()) {

                            System.out.println("Sending User Notification for counter to going ---->>>>>>"
                                    + PushNotification.TICKET_GET_TIMELINE.getNotificationTitle() + "  "
                                    + PushNotification.TICKET_GET_TIMELINE.getNotificationDescription() + "  "
                                    + userSession.getDeviceToken());

                            NotificationSender.sendPushNotificationData(
                                    PushNotification.TICKET_GET_TIMELINE.getNotificationTitle()
                                    + ticket.getTicketNumber(),
                                    PushNotification.TICKET_GET_TIMELINE.getNotificationDescription(),
                                    userSession.getDeviceToken(), metaData);
                        }
                    }
                }
            } else {
                GuestTicket guestTicket = RestUtil.getRequestNoCheck(
                        dbConnectorMicroserviceURL + "/tab/guesttickets/get/ticket?ticketId=" + ticket.getTicketId(),
                        GuestTicket.class);

                if (guestTicket != null) {
                    Map<String, String> metaData = new HashMap<String, String>();
                    metaData.put("NotificationCode", PushNotification.TICKET_GET_TIMELINE.getNotificationCode());
                    metaData.put("ticketid", guestTicket.getTicket().getTicketId().toString());
                    metaData.put("ticketNumber", guestTicket.getTicket().getTicketNumber());
                    metaData.put("ticketType", Integer.toString(guestTicket.getTicket().getTicketType()));

                    if (guestTicket.getDeviceToken() != null) {
                        if (!guestTicket.getDeviceToken().isEmpty()) {

                            NotificationSender.sendPushNotificationData(
                                    PushNotification.TICKET_GET_TIMELINE.getNotificationTitle()
                                    + ticket.getTicketNumber(),
                                    PushNotification.TICKET_GET_TIMELINE.getNotificationDescription(),
                                    guestTicket.getDeviceToken(), metaData);
                        }
                    }
                }
            }

            // send notification to counter application to move to feedback screen
            BranchCounterConfig branchCounterConfig = RestUtil.getRequestNoCheck(
                    dbConnectorMicroserviceURL + "/tab/branchcounterconfigs/get/counter?branchCounterId="
                    + ticketServed.getBranchCounter().getCounterId(),
                    BranchCounterConfig.class);

            System.out.println("A123");
            if (branchCounterConfig != null) {
                System.out.println("A123 " + branchCounterConfig.getFeedBackScreen());

                if (branchCounterConfig.getFeedBackScreen()) {
                    Map<String, String> metaData = new HashMap<String, String>();
                    metaData.put("NotificationCode", PushNotification.TICKET_FEEDBACK.getNotificationCode());
                    metaData.put("counterNumber", ticketServed.getBranchCounter().getCounterNumber());

                    NotificationSender.sendPushNotificationData(PushNotification.TICKET_FEEDBACK.getNotificationTitle(),
                            PushNotification.TICKET_FEEDBACK.getNotificationDescription(),
                            branchCounterConfig.getDeviceToken(), metaData);
                }
            }
        }

        return serveCustomerTicket();
    }

    @Override
    public GetTransferCountersResponse getTransferCounters(@Valid StepInOutTicketPayload payload)
            throws TaboorQMSServiceException, Exception {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        if (session.getUser() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.USER_NOT_EXISTS.getErrorDetails());
        }

        AgentCounterConfig agentCounterConfig = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/agentcounterconfigs/get/token?deviceToken="
                + session.getDeviceToken(),
                AgentCounterConfig.class, xyzErrorMessage.AGENTCOUNTERCONFIG_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.AGENTCOUNTERCONFIG_NOT_EXISTS.getErrorDetails());

        BranchCounter branchCounterEntity = agentCounterConfig.getBranchCounter();

        Optional<Ticket> ticketOp = ticketRepository.findById(payload.getTicketId());
        Ticket ticket = ticketOp.get();

        List<?> branchCounterList = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounterservicetabmapper/get/branchAndService?branchId="
                + ticket.getBranchId() + "&serviceId=" + ticket.getServiceId(),
                List.class, xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorDetails());

        List<BranchCounter> counterList = GenericMapper.convertListToObject(branchCounterList,
                new TypeReference<List<BranchCounter>>() {
        });

        List<BranchCounter> counters = new ArrayList<BranchCounter>();

        counterList.removeIf(bc -> bc.getCounterNumber().equals(branchCounterEntity.getCounterNumber()));

        for (BranchCounter branchCounter : counterList) {

            BranchCounterAgentMapper branchCounterAgentMapper = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                    + "/tab/branchcounteragent/get/counter?counterId=" + branchCounter.getCounterId(),
                    BranchCounterAgentMapper.class);

            if (branchCounterAgentMapper != null) {
                System.out.println(
                        "In the branchCounterAgentMapper != null loop -->>" + branchCounter.getCounterNumber());
                if (branchCounterAgentMapper.getAgent().isLoginStatus()) {
                    branchCounter.setBranch(null);
                    counters.add(branchCounter);
                } else if (session.getUser().getRoleName().equals(RoleName.Branch_Admin.name())) {
                    System.out.println("In the else if loop with counter -->>" + branchCounter.getCounterNumber());
                    branchCounter.setBranch(null);
                    counters.add(branchCounter);
                }
            }

        }

        return new GetTransferCountersResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.BRANCH_COUNTER_FETCHED.getMessage(), counters);
    }

    @Override
    public GenStatusResponse transferTicketCounter(@Valid GetIdListResponse payload)
            throws TaboorQMSServiceException, Exception {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();

        Long ticketId = payload.getIds().get(0);
        Long counterId = payload.getIds().get(1);

        BranchCounter branchCounter = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounters/get/id?branchCounterId=" + counterId,
                BranchCounter.class, xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.BRANCH_COUNTER_NOT_EXISTS.getErrorDetails());

        Optional<Ticket> ticketOp = ticketRepository.findById(ticketId);
        Ticket ticket = ticketOp.get();

        ticket.setTransferedStatus(TicketTransferedStatus.TRANSFERED.getStatus());
        ticket.setTransferedCounterId(branchCounter.getCounterId());
        ticket.setTicketStatus(TicketStatus.WAITING.getStatus());
        ticket.setAgentCallTime(null);

        ticketRepository.save(ticket);
        saveTicket(ticket);

        AgentCounterConfig agentCounterConfigEntity = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                + "/tab/agentcounterconfigs/get/token?deviceToken=" + session.getDeviceToken(),
                AgentCounterConfig.class);

        CheckAgentTicketCountPayload payload1 = new CheckAgentTicketCountPayload();
        payload1.setServingDate(LocalDate.now());
        payload1.setUserId(session.getUser().getUserId());
        payload1.setBranchCounterId(agentCounterConfigEntity.getBranchCounter().getCounterId());

        AgentTicketCount agentTicketCount = RestUtil.postRequestNoCheck(
                dbConnectorMicroserviceURL + "/tab/agentticketcounts/getByDateAndBranchAndUser", payload1,
                AgentTicketCount.class);

        agentTicketCount.setTotalTicketTransferred(agentTicketCount.getTotalTicketTransferred() + 1);

        HttpEntity<AgentTicketCount> agentTicketCountEntity = new HttpEntity<>(agentTicketCount,
                HttpHeadersUtils.getApplicationJsonHeader());
        RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/agentticketcounts/save", agentTicketCountEntity,
                AgentTicketCount.class, xyzErrorMessage.AGENTTICKETCOUNT_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.AGENTTICKETCOUNT_NOT_CREATED.getErrorDetails());

        // send notification to counters agent application to change no of tickets in
        // queue
        List<?> branchCounterList = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounterservicetabmapper/get/branchAndService?branchId="
                + ticket.getBranchId() + "&serviceId=" + ticket.getServiceId(),
                List.class, xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorDetails());

        List<BranchCounter> counterList = GenericMapper.convertListToObject(branchCounterList,
                new TypeReference<List<BranchCounter>>() {
        });

        for (BranchCounter branchCounterEntity : counterList) {
            List<?> branchCounterServiceTABs = RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/branchcounterservicetabmapper/get/branchCounter?branchCounterId="
                    + branchCounterEntity.getCounterId(),
                    List.class, xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorCode(),
                    xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorDetails());

            List<BranchCounterServiceTABMapper> branchCounterServiceList = GenericMapper.convertListToObject(
                    branchCounterServiceTABs, new TypeReference<List<BranchCounterServiceTABMapper>>() {
            });

            int sumTicketsWaitingInQueue = 0;

            for (BranchCounterServiceTABMapper bcst : branchCounterServiceList) {
                int ticketsWaitingInQueueWaiting = getNoOfTicketsInQueue(ticket.getBranchId(),
                        bcst.getServiceTAB().getServiceId(), branchCounterEntity.getCounterId());
                sumTicketsWaitingInQueue += ticketsWaitingInQueueWaiting;
            }

            AgentCounterConfig agentCounterConfig = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                    + "/tab/agentcounterconfigs/get/counter?branchCounterId=" + branchCounterEntity.getCounterId(),
                    AgentCounterConfig.class);

            if (agentCounterConfig != null) {
                Map<String, String> metaData1 = new HashMap<String, String>();
                metaData1.put("NotificationCode", PushNotification.CHANGE_TICKET_COUNT.getNotificationCode());
                metaData1.put("ticketsWaitingInQueue", String.valueOf(sumTicketsWaitingInQueue));

                NotificationSender.sendPushNotificationData(PushNotification.CHANGE_TICKET_COUNT.getNotificationTitle(),
                        PushNotification.CHANGE_TICKET_COUNT.getNotificationDescription(),
                        agentCounterConfig.getDeviceToken(), metaData1);
            }
        }

        TicketUserMapper ticketUserMapper = RestUtil.getRequestNoCheck(
                dbConnectorMicroserviceURL + "/tab/ticketusermapper/get/ticketId?ticketId=" + ticket.getTicketId(),
                TicketUserMapper.class);

        if (ticketUserMapper != null) {
            if (ticketUserMapper.getUser() != null) {

                System.out.println(
                        "In Ticket User Mapper Now ------------>>>>>> ID:" + ticketUserMapper.getUser().getUserId());

                Session userSession = RestUtil.getRequest(
                        dbConnectorMicroserviceURL + "/tab/sessions/getActiveSession/userId?userId="
                        + ticketUserMapper.getUser().getUserId(),
                        Session.class, xyzErrorMessage.SESSION_NOT_CREATED.getErrorCode(),
                        xyzErrorMessage.SESSION_NOT_CREATED.getErrorDetails());

                // send notification to customer application to move to counter to serve ticket
                Map<String, String> metaData = new HashMap<String, String>();
                metaData.put("NotificationCode", PushNotification.TICKET_SERVING_TURN.getNotificationCode());
                metaData.put("ticketNumber", ticket.getTicketNumber());
                metaData.put("ticketId", ticket.getTicketId().toString());
                metaData.put("branchId", ticket.getBranchId().toString());
                metaData.put("serviceId", ticket.getServiceId().toString());
                metaData.put("branchName", getBranch(ticket.getBranchId()).getBranchName());
                metaData.put("branchAddress", getBranch(ticket.getBranchId()).getAddress());
                metaData.put("serviceName", getServiceTAB(ticket.getServiceId()).getServiceName());

                PushUserNotificationPayload userNotificationPayload = new PushUserNotificationPayload();
                userNotificationPayload.setTitle(
                        PushNotification.TICKET_SERVING_TURN.getNotificationTitle() + ticket.getTicketNumber());
                userNotificationPayload.setDescription(PushNotification.TICKET_SERVING_TURN.getNotificationDescription()
                        + branchCounter.getCounterNumber());
                userNotificationPayload.setMetaData(metaData);

                String uri = userManagementMicroserviceURL + "/notification/push";
                HttpHeaders httpHeaders = HttpHeadersUtils.getApplicationJsonHeader();
                httpHeaders.setBearerAuth(session.getSessionToken());
                HttpEntity<PushUserNotificationPayload> userNotificationEntity = new HttpEntity<>(
                        userNotificationPayload, httpHeaders);
                ResponseEntity<GenStatusResponse> pushNotificationResponse = restTemplate.postForEntity(uri,
                        userNotificationEntity, GenStatusResponse.class);

                if (!pushNotificationResponse.getStatusCode().equals(HttpStatus.OK) || pushNotificationResponse
                        .getBody().getApplicationStatusCode() == xyzResponseCode.ERROR.getCode()) {
                    throw new TaboorQMSServiceException(
                            444 + "::" + pushNotificationResponse.getBody().getApplicationStatusResponse());
                }

                if (userSession.getDeviceToken() != null) {
                    if (!userSession.getDeviceToken().isEmpty()) {

                        System.out.println("Sending User Notification for counter to going ---->>>>>>"
                                + userNotificationPayload.getTitle() + "  " + userNotificationPayload.getDescription()
                                + "  " + userSession.getDeviceToken());

                        NotificationSender.sendPushNotificationData(userNotificationPayload.getTitle(),
                                userNotificationPayload.getDescription(), userSession.getDeviceToken(), metaData);
                    }
                }
            }
        } else {
            GuestTicket guestTicket = RestUtil.getRequestNoCheck(
                    dbConnectorMicroserviceURL + "/tab/guesttickets/get/ticket?ticketId=" + ticket.getTicketId(),
                    GuestTicket.class);

            if (guestTicket != null) {
                // send notification to customer application to move to counter to serve ticket
                Map<String, String> metaData = new HashMap<String, String>();
                metaData.put("NotificationCode", PushNotification.TICKET_SERVING_TURN.getNotificationCode());
                metaData.put("ticketNumber", ticket.getTicketNumber());
                metaData.put("ticketId", ticket.getTicketId().toString());
                metaData.put("branchId", ticket.getBranchId().toString());
                metaData.put("serviceId", ticket.getServiceId().toString());
                metaData.put("branchName", getBranch(ticket.getBranchId()).getBranchName());
                metaData.put("branchAddress", getBranch(ticket.getBranchId()).getAddress());
                metaData.put("serviceName", getServiceTAB(ticket.getServiceId()).getServiceName());

                PushUserNotificationPayload userNotificationPayload = new PushUserNotificationPayload();
                userNotificationPayload.setTitle(
                        PushNotification.TICKET_SERVING_TURN.getNotificationTitle() + ticket.getTicketNumber());
                userNotificationPayload.setDescription(PushNotification.TICKET_SERVING_TURN.getNotificationDescription()
                        + branchCounter.getCounterNumber());
                userNotificationPayload.setMetaData(metaData);

                String uri = userManagementMicroserviceURL + "/notification/push";
                HttpHeaders httpHeaders = HttpHeadersUtils.getApplicationJsonHeader();
                httpHeaders.setBearerAuth(session.getSessionToken());
                HttpEntity<PushUserNotificationPayload> userNotificationEntity = new HttpEntity<>(
                        userNotificationPayload, httpHeaders);
                ResponseEntity<GenStatusResponse> pushNotificationResponse = restTemplate.postForEntity(uri,
                        userNotificationEntity, GenStatusResponse.class);

                if (!pushNotificationResponse.getStatusCode().equals(HttpStatus.OK) || pushNotificationResponse
                        .getBody().getApplicationStatusCode() == xyzResponseCode.ERROR.getCode()) {
                    throw new TaboorQMSServiceException(
                            444 + "::" + pushNotificationResponse.getBody().getApplicationStatusResponse());
                }

                if (guestTicket.getDeviceToken() != null) {
                    if (!guestTicket.getDeviceToken().isEmpty()) {

                        NotificationSender.sendPushNotificationData(userNotificationPayload.getTitle(),
                                userNotificationPayload.getDescription(), guestTicket.getDeviceToken(), metaData);
                    }
                }
            }
        }

        // send notification to counter application to change the serving ticket to OPEN
        BranchCounterConfig branchCounterConfig = RestUtil
                .getRequestNoCheck(
                        dbConnectorMicroserviceURL + "/tab/branchcounterconfigs/get/counter?branchCounterId="
                        + agentCounterConfigEntity.getBranchCounter().getCounterId(),
                        BranchCounterConfig.class);

        if (branchCounterConfig != null) {
            Map<String, String> metaData = new HashMap<String, String>();
            metaData.put("NotificationCode", PushNotification.CHANGE_AGENT_STATUS.getNotificationCode());
            metaData.put("CounterNumber", branchCounterConfig.getBranchCounter().getCounterNumber().toString());
            metaData.put("CounterStatus", Boolean.TRUE.toString());

            NotificationSender.sendPushNotificationData(PushNotification.CHANGE_AGENT_STATUS.getNotificationTitle(),
                    PushNotification.CHANGE_AGENT_STATUS.getNotificationDescription(),
                    branchCounterConfig.getDeviceToken(), metaData);
        }

        // send notification to TV application to change the serving ticket to OPEN
        BranchTVConfig branchTVConfig = RestUtil
                .getRequestNoCheck(
                        dbConnectorMicroserviceURL + "/tab/branchtvconfigs/get/branch?branchId="
                        + agentCounterConfigEntity.getBranchCounter().getBranch().getBranchId(),
                        BranchTVConfig.class);

        if (branchTVConfig != null) {

            Map<String, String> metaData2 = new HashMap<String, String>();
            metaData2.put("NotificationCode", PushNotification.CHANGE_AGENT_STATUS.getNotificationCode());
            metaData2.put("CounterNumber", agentCounterConfigEntity.getBranchCounter().getCounterNumber().toString());
            metaData2.put("CounterStatus", Boolean.TRUE.toString());

            NotificationSender.sendPushNotificationData(PushNotification.CHANGE_AGENT_STATUS.getNotificationTitle(),
                    PushNotification.CHANGE_AGENT_STATUS.getNotificationDescription(), branchTVConfig.getDeviceToken(),
                    metaData2);
        }

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.TICKET_TRANSFERED.getMessage());
    }

    @Override
    public GenStatusResponse recallTicket(@Valid GetIdListResponse payload)
            throws TaboorQMSServiceException, Exception {

        Long ticketId = payload.getIds().get(0);
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();
        if (session.getUser() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.USER_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.USER_NOT_EXISTS.getErrorDetails());
        }

        AgentCounterConfig agentCounterConfig = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/agentcounterconfigs/get/token?deviceToken="
                + session.getDeviceToken(),
                AgentCounterConfig.class, xyzErrorMessage.AGENTCOUNTERCONFIG_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.AGENTCOUNTERCONFIG_NOT_EXISTS.getErrorDetails());

        BranchCounter branchCounter = agentCounterConfig.getBranchCounter();

        TicketUserMapper ticketUserMapper = RestUtil.getRequestNoCheck(
                dbConnectorMicroserviceURL + "/tab/ticketusermapper/get/ticketId?ticketId=" + ticketId,
                TicketUserMapper.class);
        AgentCounterConfig agentCounterConfigEntity = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                + "/tab/agentcounterconfigs/get/token?deviceToken=" + session.getDeviceToken(),
                AgentCounterConfig.class);

        CheckAgentTicketCountPayload payload1 = new CheckAgentTicketCountPayload();
        payload1.setServingDate(LocalDate.now());
        payload1.setUserId(session.getUser().getUserId());
        payload1.setBranchCounterId(agentCounterConfigEntity.getBranchCounter().getCounterId());

        AgentTicketCount agentTicketCount = RestUtil.postRequestNoCheck(
                dbConnectorMicroserviceURL + "/tab/agentticketcounts/getByDateAndBranchAndUser", payload1,
                AgentTicketCount.class);

        agentTicketCount.setTotalTicketRecalled(agentTicketCount.getTotalTicketRecalled() + 1);
        if (ticketUserMapper != null) {
            if (ticketUserMapper.getUser() != null) {

                Session userSession = RestUtil.getRequest(
                        dbConnectorMicroserviceURL + "/tab/sessions/getActiveSession/userId?userId="
                        + ticketUserMapper.getUser().getUserId(),
                        Session.class, xyzErrorMessage.SESSION_NOT_CREATED.getErrorCode(),
                        xyzErrorMessage.SESSION_NOT_CREATED.getErrorDetails());

                // send notification to customer application to move to counter to serve ticket
                Map<String, String> metaData = new HashMap<String, String>();
                switch (agentTicketCount.getTotalTicketRecalled()) {
                    case 1:
                        metaData.put("NotificationCode", PushNotification.TICKET_RECALL_1.getNotificationCode());
                        break;
                    case 2:
                        metaData.put("NotificationCode", PushNotification.TICKET_RECALL_2.getNotificationCode());
                        break;
                    case 3:
                        metaData.put("NotificationCode", PushNotification.TICKET_RECALL_3.getNotificationCode());
                        break;

                }

                metaData.put("ticketNumber", ticketUserMapper.getTicket().getTicketNumber());
                metaData.put("ticketId", ticketUserMapper.getTicket().getTicketId().toString());
                metaData.put("branchId", ticketUserMapper.getTicket().getBranch().getBranchId().toString());
                metaData.put("serviceId", ticketUserMapper.getTicket().getService().getServiceId().toString());
                metaData.put("branchName", ticketUserMapper.getTicket().getBranch().getBranchName());
                metaData.put("branchAddress", ticketUserMapper.getTicket().getBranch().getAddress());
                metaData.put("serviceName", ticketUserMapper.getTicket().getService().getServiceName());

                PushUserNotificationPayload userNotificationPayload = new PushUserNotificationPayload();
                switch (agentTicketCount.getTotalTicketRecalled()) {
                    case 1:
                        userNotificationPayload.setTitle(PushNotification.TICKET_RECALL_1.getNotificationTitle()
                                + ticketUserMapper.getTicket().getTicketNumber());
                        userNotificationPayload.setDescription(PushNotification.TICKET_RECALL_1.getNotificationDescription()
                                + branchCounter.getCounterNumber());
                        userNotificationPayload.setMetaData(metaData);
                        break;
                    case 2:
                        userNotificationPayload.setTitle(PushNotification.TICKET_RECALL_2.getNotificationTitle()
                                + ticketUserMapper.getTicket().getTicketNumber());
                        userNotificationPayload.setDescription(PushNotification.TICKET_RECALL_2.getNotificationDescription()
                                + branchCounter.getCounterNumber());
                        userNotificationPayload.setMetaData(metaData);
                        break;
                    case 3:
                        userNotificationPayload.setTitle(PushNotification.TICKET_RECALL_3.getNotificationTitle()
                                + ticketUserMapper.getTicket().getTicketNumber());
                        userNotificationPayload.setDescription(PushNotification.TICKET_RECALL_3.getNotificationDescription()
                                + branchCounter.getCounterNumber());
                        userNotificationPayload.setMetaData(metaData);
                        break;

                }

                String uri = userManagementMicroserviceURL + "/notification/push";
                HttpHeaders httpHeaders = HttpHeadersUtils.getApplicationJsonHeader();
                httpHeaders.setBearerAuth(session.getSessionToken());
                HttpEntity<PushUserNotificationPayload> userNotificationEntity = new HttpEntity<>(
                        userNotificationPayload, httpHeaders);
                ResponseEntity<GenStatusResponse> pushNotificationResponse = restTemplate.postForEntity(uri,
                        userNotificationEntity, GenStatusResponse.class);

                if (!pushNotificationResponse.getStatusCode().equals(HttpStatus.OK) || pushNotificationResponse
                        .getBody().getApplicationStatusCode() == xyzResponseCode.ERROR.getCode()) {
                    throw new TaboorQMSServiceException(
                            444 + "::" + pushNotificationResponse.getBody().getApplicationStatusResponse());
                }

                if (userSession.getDeviceToken() != null) {
                    if (!userSession.getDeviceToken().isEmpty()) {
                        NotificationSender.sendPushNotificationData(userNotificationPayload.getTitle(),
                                userNotificationPayload.getDescription(), userSession.getDeviceToken(), metaData);
                    }
                }
            }
        } else {
            GuestTicket guestTicket = RestUtil.getRequestNoCheck(
                    dbConnectorMicroserviceURL + "/tab/guesttickets/get/ticket?ticketId=" + ticketId,
                    GuestTicket.class);

            if (guestTicket != null) {
                // send notification to customer application to move to counter to serve ticket
                Map<String, String> metaData = new HashMap<String, String>();
                switch (agentTicketCount.getTotalTicketRecalled()) {
                    case 1:
                        metaData.put("NotificationCode", PushNotification.TICKET_RECALL_1.getNotificationCode());
                        break;
                    case 2:
                        metaData.put("NotificationCode", PushNotification.TICKET_RECALL_2.getNotificationCode());
                        break;
                    case 3:
                        metaData.put("NotificationCode", PushNotification.TICKET_RECALL_3.getNotificationCode());
                        break;

                }
                metaData.put("ticketNumber", guestTicket.getTicket().getTicketNumber());
                metaData.put("ticketId", guestTicket.getTicket().getTicketId().toString());
                metaData.put("branchId", guestTicket.getTicket().getBranch().getBranchId().toString());
                metaData.put("serviceId", guestTicket.getTicket().getService().getServiceId().toString());
                metaData.put("branchName", guestTicket.getTicket().getBranch().getBranchName());
                metaData.put("branchAddress", guestTicket.getTicket().getBranch().getAddress());
                metaData.put("serviceName", guestTicket.getTicket().getService().getServiceName());

                PushUserNotificationPayload userNotificationPayload = new PushUserNotificationPayload();
                switch (agentTicketCount.getTotalTicketRecalled()) {
                    case 1:
                        userNotificationPayload.setTitle(PushNotification.TICKET_RECALL_1.getNotificationTitle()
                                + ticketUserMapper.getTicket().getTicketNumber());
                        userNotificationPayload.setDescription(PushNotification.TICKET_RECALL_1.getNotificationDescription()
                                + branchCounter.getCounterNumber());
                        userNotificationPayload.setMetaData(metaData);
                        break;
                    case 2:
                        userNotificationPayload.setTitle(PushNotification.TICKET_RECALL_2.getNotificationTitle()
                                + ticketUserMapper.getTicket().getTicketNumber());
                        userNotificationPayload.setDescription(PushNotification.TICKET_RECALL_2.getNotificationDescription()
                                + branchCounter.getCounterNumber());
                        userNotificationPayload.setMetaData(metaData);
                        break;
                    case 3:
                        userNotificationPayload.setTitle(PushNotification.TICKET_RECALL_3.getNotificationTitle()
                                + ticketUserMapper.getTicket().getTicketNumber());
                        userNotificationPayload.setDescription(PushNotification.TICKET_RECALL_3.getNotificationDescription()
                                + branchCounter.getCounterNumber());
                        userNotificationPayload.setMetaData(metaData);
                        break;

                }

                userNotificationPayload.setMetaData(metaData);

                String uri = userManagementMicroserviceURL + "/notification/push";
                HttpHeaders httpHeaders = HttpHeadersUtils.getApplicationJsonHeader();
                httpHeaders.setBearerAuth(session.getSessionToken());
                HttpEntity<PushUserNotificationPayload> userNotificationEntity = new HttpEntity<>(
                        userNotificationPayload, httpHeaders);
                ResponseEntity<GenStatusResponse> pushNotificationResponse = restTemplate.postForEntity(uri,
                        userNotificationEntity, GenStatusResponse.class);

                if (!pushNotificationResponse.getStatusCode().equals(HttpStatus.OK) || pushNotificationResponse
                        .getBody().getApplicationStatusCode() == xyzResponseCode.ERROR.getCode()) {
                    throw new TaboorQMSServiceException(
                            444 + "::" + pushNotificationResponse.getBody().getApplicationStatusResponse());
                }

                if (guestTicket.getDeviceToken() != null) {
                    if (!guestTicket.getDeviceToken().isEmpty()) {
                        NotificationSender.sendPushNotificationData(userNotificationPayload.getTitle(),
                                userNotificationPayload.getDescription(), guestTicket.getDeviceToken(), metaData);
                    }
                }
            }
        }

        HttpEntity<AgentTicketCount> agentTicketCountEntity = new HttpEntity<>(agentTicketCount,
                HttpHeadersUtils.getApplicationJsonHeader());
        RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/agentticketcounts/save", agentTicketCountEntity,
                AgentTicketCount.class, xyzErrorMessage.AGENTTICKETCOUNT_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.AGENTTICKETCOUNT_NOT_CREATED.getErrorDetails());

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.TICKET_RECALL.getMessage());
    }

    @Override
    public GenStatusResponse logoutAgent() throws TaboorQMSServiceException, Exception {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();

        System.out.println("Y " + session);
        System.out.println("Z " + session.getDeviceToken());

        AgentCounterConfig agentCounterConfig = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/agentcounterconfigs/get/token?deviceToken="
                + session.getDeviceToken(),
                AgentCounterConfig.class, xyzErrorMessage.AGENTCOUNTERCONFIG_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.AGENTCOUNTERCONFIG_NOT_EXISTS.getErrorDetails());

        BranchCounter branchCounter = agentCounterConfig.getBranchCounter();

        BranchCounterAgentMapper branchCounterAgentMapper = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                + "/tab/branchcounteragent/get/counter?counterId=" + branchCounter.getCounterId(),
                BranchCounterAgentMapper.class);

        if (branchCounterAgentMapper != null && branchCounterAgentMapper.getAgent() != null && branchCounterAgentMapper
                .getAgent().getServiceCenterEmployee().getUser().getEmail().equals(session.getUser().getEmail())) {
            branchCounterAgentMapper.getAgent().setLoginStatus(false);
            long hoursDifference = branchCounterAgentMapper.getAgent().getServiceCenterEmployee().getUser()
                    .getLastLoginTime().until(OffsetDateTime.now(), ChronoUnit.HOURS);
            long minutesDifference = branchCounterAgentMapper.getAgent().getServiceCenterEmployee().getUser()
                    .getLastLoginTime().until(OffsetDateTime.now(), ChronoUnit.MINUTES);
            LocalTime totalHours = branchCounterAgentMapper.getAgent().getTotalWorkHours().plusHours(hoursDifference)
                    .plusMinutes(minutesDifference);
            branchCounterAgentMapper.getAgent().setTotalWorkHours(totalHours);

            HttpEntity<Agent> entity1 = new HttpEntity<>(branchCounterAgentMapper.getAgent(),
                    HttpHeadersUtils.getApplicationJsonHeader());
            RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/agents/save", entity1, Agent.class,
                    xyzErrorMessage.AGENT_NOT_UPDATED.getErrorCode(),
                    xyzErrorMessage.AGENT_NOT_UPDATED.getErrorDetails());
        }

        // send notification to counter application to change the counter to CLOSE
        BranchCounterConfig branchCounterConfig = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                + "/tab/branchcounterconfigs/get/counter?branchCounterId=" + branchCounter.getCounterId(),
                BranchCounterConfig.class);

        if (branchCounterConfig != null) {
            Map<String, String> metaData = new HashMap<String, String>();
            metaData.put("NotificationCode", PushNotification.CHANGE_AGENT_STATUS.getNotificationCode());
            metaData.put("CounterNumber", branchCounter.getCounterNumber().toString());
            metaData.put("CounterStatus", Boolean.FALSE.toString());

            NotificationSender.sendPushNotificationData(PushNotification.CHANGE_AGENT_STATUS.getNotificationTitle(),
                    PushNotification.CHANGE_AGENT_STATUS.getNotificationDescription(),
                    branchCounterConfig.getDeviceToken(), metaData);
        }

        // send notification to TV application to change the serving counter to CLOSE
        BranchTVConfig branchTVConfig = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                + "/tab/branchtvconfigs/get/branch?branchId=" + branchCounter.getBranch().getBranchId(),
                BranchTVConfig.class);

        if (branchTVConfig != null) {
            Map<String, String> metaData = new HashMap<String, String>();
            metaData.put("NotificationCode", PushNotification.CHANGE_AGENT_STATUS.getNotificationCode());
            metaData.put("CounterNumber", branchCounter.getCounterNumber().toString());
            metaData.put("CounterStatus", Boolean.FALSE.toString());

            NotificationSender.sendPushNotificationData(PushNotification.CHANGE_AGENT_STATUS.getNotificationTitle(),
                    PushNotification.CHANGE_AGENT_STATUS.getNotificationDescription(), branchTVConfig.getDeviceToken(),
                    metaData);
        }

//		RestUtil.getRequest(
//				dbConnectorMicroserviceURL + "/tab/agentcounterconfigs/delete/deviceToken?deviceToken="
//						+ session.getDeviceToken(),
//				Boolean.class, xyzErrorMessage.TVCONFIG_NOT_EXISTS.getErrorCode(),
//				xyzErrorMessage.TVCONFIG_NOT_EXISTS.getErrorDetails());
        session.setLogoutTime(OffsetDateTime.now());
        HttpEntity<Session> entity = new HttpEntity<>(session, HttpHeadersUtils.getApplicationJsonHeader());
        RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/sessions/save", entity, Session.class,
                xyzErrorMessage.USER_NOT_LOGOUT.getErrorCode(), xyzErrorMessage.USER_NOT_LOGOUT.getErrorDetails());

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(), xyzResponseMessage.USER_LOGOUT.getMessage());
    }

    @Override
    public GetAgentListResponse getDetails() throws TaboorQMSServiceException, Exception {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();

        AgentCounterConfig agentCounterConfig = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/agentcounterconfigs/get/token?deviceToken="
                + session.getDeviceToken(),
                AgentCounterConfig.class, xyzErrorMessage.AGENTCOUNTERCONFIG_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.AGENTCOUNTERCONFIG_NOT_EXISTS.getErrorDetails());

        BranchCounter branchCounter = agentCounterConfig.getBranchCounter();

        BranchCounterAgentMapper branchCounterAgentMapper = RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounteragent/get/counter?counterId="
                + branchCounter.getCounterId(),
                BranchCounterAgentMapper.class, xyzErrorMessage.BRANCH_COUNTER_AGENT_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.BRANCH_COUNTER_AGENT_NOT_EXISTS.getErrorDetails());

        String uri = dbConnectorMicroserviceURL + "/tab/agents/getDeatils/id?agentId="
                + branchCounterAgentMapper.getAgent().getAgentId();
        ResponseEntity<AgentObject> getAgentDetailsResponse = restTemplate.getForEntity(uri, AgentObject.class);
        if (!getAgentDetailsResponse.getStatusCode().equals(HttpStatus.OK) || getAgentDetailsResponse.getBody() == null) {
            throw new TaboorQMSServiceException(xyzErrorMessage.AGENT_NOT_EXISTS.getErrorCode() + "::"
                    + xyzErrorMessage.AGENT_NOT_EXISTS.getErrorDetails());
        }

        getAgentDetailsResponse.getBody().getAgent().getBranch().setServiceCenter(null);
        getAgentDetailsResponse.getBody().getAgent().getServiceCenterEmployee().setServiceCenter(null);
        if (getAgentDetailsResponse.getBody().getAssignedBranchCounter() != null) {
            getAgentDetailsResponse.getBody().getAssignedBranchCounter().setBranch(null);
        }

        return new GetAgentListResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.AGENT_FETCHED.getMessage(), Arrays.asList(getAgentDetailsResponse.getBody()));

    }

    @Override
    public GetStatisticsResponse getStatistics(GetActivitiesPayload payload)
            throws TaboorQMSServiceException, Exception {


        Period period = Period.between(payload.getStartDate(), payload.getEndDate());

        List<?> response = RestUtil.postRequest(dbConnectorMicroserviceURL + "/tab/ticketfeedbacks/countRatings/agent",
                new HttpEntity<>(payload, HttpHeadersUtils.getApplicationJsonHeader()), List.class,
                xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorCode(),
                xyzErrorMessage.DBCONNECTOR_SERVICE_ERROR.getErrorDetails());

        List<ValueObject> valueObjects = new ArrayList<ValueObject>();
        valueObjects.addAll(GenericMapper.convertListToObject(response, new TypeReference<List<ValueObject>>() {
        }));

        if (period.getYears() > 0) {
            // breakdown into years
            LocalDate startYear = payload.getStartDate(),
                    endYear = payload.getStartDate().plusYears(1).withDayOfYear(1).minusDays(1);

            for (int i = 0; i <= period.getYears(); i++, startYear = endYear.plusDays(1), endYear = startYear
                    .plusYears(1).minusDays(1)) {
                if (endYear.isAfter(payload.getEndDate())) {
                    endYear = payload.getEndDate();
                }
                valueObjects.add(new ValueObject(String.valueOf(startYear.getYear()),
                        String.valueOf(getAgentStats(payload.getIdList().get(0), startYear, endYear))));
            }
        } else if (period.getMonths() > 0) {
            // breakdown into months
            LocalDate startMonth = payload.getStartDate(),
                    endMonth = payload.getStartDate().plusMonths(1).withDayOfMonth(1).minusDays(1);

            for (int i = 0; i <= period.getMonths(); i++, startMonth = endMonth.plusDays(1), endMonth = startMonth
                    .plusMonths(1).minusDays(1)) {
                if (endMonth.isAfter(payload.getEndDate())) {
                    endMonth = payload.getEndDate();
                }
                valueObjects.add(new ValueObject(
                        String.valueOf(startMonth.getMonth().getDisplayName(TextStyle.SHORT, Locale.US)),
                        String.valueOf(getAgentStats(payload.getIdList().get(0), startMonth, endMonth))));
            }
        } else if (period.getDays() > 6) {
            // breakdown into weeks
            LocalDate startWeek = payload.getStartDate(), endWeek = payload.getStartDate().plusWeeks(1).minusDays(1);
            int i = 1;
            while (true) {
                if (endWeek.isAfter(payload.getEndDate())) {
                    endWeek = payload.getEndDate();
                }
                valueObjects.add(new ValueObject("W-" + i++,
                        String.valueOf(getAgentStats(payload.getIdList().get(0), startWeek, endWeek))));
                if (!endWeek.isBefore(payload.getEndDate())) {
                    break;
                }
                startWeek = endWeek.plusDays(1);
                endWeek = startWeek.plusWeeks(1).minusDays(1);
            }
        } else if (period.getDays() > 0) {
            // breakdown into days
            for (LocalDate currentDate = payload.getStartDate(); payload.getEndDate().plusDays(1)
                    .isAfter(currentDate); currentDate = currentDate.plusDays(1)) {
                valueObjects.add(new ValueObject(
                        String.valueOf(currentDate.getDayOfWeek().getDisplayName(TextStyle.NARROW, Locale.US)),
                        String.valueOf(getAgentStats(payload.getIdList().get(0), currentDate, currentDate))));
            }
        }
        return new GetStatisticsResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.AGENT_STATISTICS_FETHCED.getMessage(), valueObjects);
    }

    private Double getAgentStats(Long agentId, LocalDate startDate, LocalDate endDate)
            throws RestClientException, TaboorQMSServiceException {
        return RestUtil.post(dbConnectorMicroserviceURL + "/tab/ticketfeedbacks/countAvgRatings/agent",
                new HttpEntity<>(new GetActivitiesPayload(startDate, endDate, Arrays.asList(agentId)),
                        HttpHeadersUtils.getApplicationJsonHeader()),
                Double.class);
    }

//    @Override
//    public GetStatisticsResponse getStatistics(GetActivitiesPayload payload)
//            throws TaboorQMSServiceException, Exception {
//
//        Period period = Period.between(payload.getStartDate(), payload.getEndDate());
//        List<ValueObject> valueObjects = new ArrayList<ValueObject>();
//
//        if (period.getYears() > 0) {
//            // breakdown into years
//            LocalDate startYear = payload.getStartDate(),
//                    endYear = payload.getStartDate().plusYears(1).withDayOfYear(1).minusDays(1);
//
//            for (int i = 0; i <= period.getYears(); i++, startYear = endYear.plusDays(1), endYear = startYear
//                    .plusYears(1).minusDays(1)) {
//                if (endYear.isAfter(payload.getEndDate())) {
//                    endYear = payload.getEndDate();
//                }
//                valueObjects.add(new ValueObject(String.valueOf(startYear.getYear()),
//                        String.valueOf(getAgentStats(payload.getIdList().get(0), startYear, endYear))));
//            }
//        } else if (period.getMonths() > 0) {
//            // breakdown into months
//            LocalDate startMonth = payload.getStartDate(),
//                    endMonth = payload.getStartDate().plusMonths(1).withDayOfMonth(1).minusDays(1);
//
//            for (int i = 0; i <= period.getMonths(); i++, startMonth = endMonth.plusDays(1), endMonth = startMonth
//                    .plusMonths(1).minusDays(1)) {
//                if (endMonth.isAfter(payload.getEndDate())) {
//                    endMonth = payload.getEndDate();
//                }
//                valueObjects.add(new ValueObject(String.valueOf(startMonth.getMonth()),
//                        String.valueOf(getAgentStats(payload.getIdList().get(0), startMonth, endMonth))));
//            }
//        } else if (period.getDays() > 6) {
//            // breakdown into weeks
//            LocalDate startWeek = payload.getStartDate(), endWeek = payload.getStartDate().plusWeeks(1).minusDays(1);
//            int i = 1;
//            while (true) {
//                if (endWeek.isAfter(payload.getEndDate())) {
//                    endWeek = payload.getEndDate();
//                }
//                valueObjects.add(new ValueObject("Week-" + i++,
//                        String.valueOf(getAgentStats(payload.getIdList().get(0), startWeek, endWeek))));
//                if (!endWeek.isBefore(payload.getEndDate())) {
//                    break;
//                }
//                startWeek = endWeek.plusDays(1);
//                endWeek = startWeek.plusWeeks(1).minusDays(1);
//            }
//        } else if (period.getDays() > 0) {
//            // breakdown into days
//            for (LocalDate currentDate = payload.getStartDate(); payload.getEndDate().plusDays(1)
//                    .isAfter(currentDate); currentDate = currentDate.plusDays(1)) {
//                valueObjects.add(new ValueObject(String.valueOf(currentDate.getDayOfWeek().name()),
//                        String.valueOf(getAgentStats(payload.getIdList().get(0), currentDate, currentDate))));
//            }
//        }
//        return new GetStatisticsResponse(xyzResponseCode.SUCCESS.getCode(),
//                xyzResponseMessage.AGENT_STATISTICS_FETHCED.getMessage(), valueObjects);
//    }
//
//    private int getAgentStats(Long agentId, LocalDate startDate, LocalDate endDate) {
////		String uri = dbConnectorMicroserviceURL + "/tab/ticketfeedbacks/countAvgRatings/agent";
////		HttpEntity<GetActivitiesPayload> entity = new HttpEntity<>(
////				new GetActivitiesPayload(startDate, endDate, Arrays.asList(agentId)),
////				HttpHeadersUtils.getApplicationJsonHeader());
////		ResponseEntity<Double> countTicketByBranchResponse = restTemplate.postForEntity(uri, entity, Double.class);
////		if (countTicketByBranchResponse.getBody() == null)
////			return 0.0;
////		return countTicketByBranchResponse.getBody();
//
//        Random random = new Random();
//        return random.nextInt(5) + 1;
//    }
    private GetQueueTicketListResponse getSpecificQueueTickets(Long branchId, Long serviceId)
            throws TaboorQMSServiceException, Exception {

        System.out.println("Params BranchId -->>" + branchId);
        System.out.println("Params ServiceId -->>" + serviceId);

        // For H2
        Queue queue = getByBranchAndService(branchId, serviceId);

        List<QueueTicketMapper> queueTicketMapperList = queueTicketMapperRepository.findByBranchAndService(branchId,
                serviceId);
        List<Ticket> ticketListSorted = new ArrayList<Ticket>();
        for (int i = 0; i < queueTicketMapperList.size(); i++) {
            ticketListSorted.add(queueTicketMapperList.get(i).getTicket());
        }

        // TODO
        Collections.sort(ticketListSorted, Comparator.comparing(Ticket::getTransferedStatus)
                .thenComparing(Ticket::getTicketType).thenComparing(Ticket::getPositionTime));

        System.out.println("ticketListSorted Size After Sorting -->>" + ticketListSorted.size());

        List<TicketObject> ticketList = new ArrayList<GetQueueTicketListResponse.TicketObject>();
        int j = 1;

        for (Ticket ticketObj : ticketListSorted) {
            if (ticketObj.getTicketStatus() == TicketStatus.WAITING.getStatus()) {
                ticketObj.setWaitingTime(
                        LocalTime.of(0, 0, 0).plusSeconds(queue.getAverageServiceTime().toSecondOfDay() * (j++)));
                ticketList.add(new TicketObject(ticketObj, null));
            } else if (ticketObj.getCounterNumber() != null) {
                String uri = dbConnectorMicroserviceURL + "/tab/ticketserveds/getAgent/ticket?ticketId="
                        + ticketObj.getTicketId();
                ResponseEntity<Agent> getAgentResponse = restTemplate.getForEntity(uri, Agent.class);
                ticketList.add(new TicketObject(ticketObj, getAgentResponse.getBody()));
            } else {
                ticketList.add(new TicketObject(ticketObj, null));
            }
        }

        return new GetQueueTicketListResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.QUEUE_TICKET_FETCHED.getMessage(), queue, ticketList);

    }

    private Queue getByBranchAndService(Long branchId, Long serviceId) throws TaboorQMSServiceException, Exception {

        Optional<Queue> queue = queueRepository.findByBranchAndServiceAndType(branchId, serviceId);
        if (!queue.isPresent()) {
            com.taboor.qms.core.model.Queue queuePostgres = RestUtil.getRequest(
                    dbConnectorMicroserviceURL + "/tab/queues/get/branchAndService?branchId=" + branchId + "&serviceId="
                    + serviceId,
                    com.taboor.qms.core.model.Queue.class, xyzErrorMessage.QUEUE_NOT_EXISTS.getErrorCode(),
                    xyzErrorMessage.QUEUE_NOT_EXISTS.getErrorDetails());
            if (queuePostgres != null) {
                Queue q = new Queue();
                q.setQueueId(queuePostgres.getQueueId());
                q.setAverageServiceTime(queuePostgres.getAverageServiceTime());
                q.setAverageWaitTime(queuePostgres.getAverageWaitTime());
                q.setBranchId(queuePostgres.getBranch().getBranchId());
                q.setQueueNumber(queuePostgres.getQueueNumber());
                q.setServiceId(queuePostgres.getService().getServiceId());
                return q;
            } else {
                throw new TaboorQMSServiceException(xyzErrorMessage.QUEUE_NOT_EXISTS.getErrorCode() + "::"
                        + xyzErrorMessage.QUEUE_NOT_EXISTS.getErrorDetails());
            }
        }
        return queue.get();
    }

    @Override
    public TicketSummaryResponse getAgentTicketsSummary() throws TaboorQMSServiceException, Exception {

        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        Session session = myUserDetails.getSession();

        AgentCounterConfig agentCounterConfigEntity = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                + "/tab/agentcounterconfigs/get/token?deviceToken=" + session.getDeviceToken(),
                AgentCounterConfig.class);

        CheckAgentTicketCountPayload payload = new CheckAgentTicketCountPayload();
        payload.setServingDate(LocalDate.now());
        payload.setUserId(session.getUser().getUserId());
        payload.setBranchCounterId(agentCounterConfigEntity.getBranchCounter().getCounterId());

        AgentTicketCount agentTicketCount = RestUtil.postRequestNoCheck(
                dbConnectorMicroserviceURL + "/tab/agentticketcounts/getByDateAndBranchAndUser", payload,
                AgentTicketCount.class);

        if (agentTicketCount == null) {
            agentTicketCount = new AgentTicketCount();
            agentTicketCount.setServingAgent(session.getUser());
            agentTicketCount.setServingCounter(agentCounterConfigEntity.getBranchCounter());
            agentTicketCount.setServingDate(LocalDate.now());

            HttpEntity<AgentTicketCount> agentTicketCountEntity = new HttpEntity<>(agentTicketCount,
                    HttpHeadersUtils.getApplicationJsonHeader());
            agentTicketCount = RestUtil.postRequestEntity(dbConnectorMicroserviceURL + "/tab/agentticketcounts/save",
                    agentTicketCountEntity, AgentTicketCount.class,
                    xyzErrorMessage.AGENTTICKETCOUNT_NOT_CREATED.getErrorCode(),
                    xyzErrorMessage.AGENTTICKETCOUNT_NOT_CREATED.getErrorDetails());
        }

        return new TicketSummaryResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.AGENT_TICKET_SUMMARY.getMessage(), agentTicketCount.getTotalTicketServed(),
                agentTicketCount.getTotalTicketCancelled(), agentTicketCount.getTotalTicketTransferred(),
                agentTicketCount.getTotalTicketRecalled());
    }

    @Override
    public GenStatusResponse sendNoOfTicketsInQueueNotif(NewCounterPayload payload)
            throws TaboorQMSServiceException, Exception {

        Random random = new Random();
        int tickets = random.nextInt(33) + 1;

        Map<String, String> metaData = new HashMap<String, String>();
        metaData.put("NotificationCode", PushNotification.CHANGE_TICKET_COUNT.getNotificationCode());
        metaData.put("ticketsWaitingInQueue", String.valueOf(tickets));

        NotificationSender.sendPushNotificationData(PushNotification.CHANGE_TICKET_COUNT.getNotificationTitle(),
                PushNotification.CHANGE_TICKET_COUNT.getNotificationDescription(), payload.getDeviceToken(), metaData);

        return new GenStatusResponse(xyzResponseCode.SUCCESS.getCode(),
                xyzResponseMessage.TV_CONFIG_ADDED.getMessage());
    }

    @Override
    public AddAgentCounterConfigResponse checkConfiguration(NewCounterPayload payload)
            throws TaboorQMSServiceException, Exception {

        AgentCounterConfig agentCounterConfigEntity = RestUtil.getRequestNoCheck(dbConnectorMicroserviceURL
                + "/tab/agentcounterconfigs/get/token?deviceToken=" + payload.getDeviceToken(),
                AgentCounterConfig.class);

        if (agentCounterConfigEntity != null) {
            return new AddAgentCounterConfigResponse(xyzResponseCode.SUCCESS.getCode(),
                    xyzResponseMessage.AGENTCOUNTER_CONFIG_EXISTS.getMessage(),
                    agentCounterConfigEntity.getBranchCounter().getBranch().getBranchId().toString(),
                    agentCounterConfigEntity.getBranchCounter().getCounterNumber(), true);
        } else {
            return new AddAgentCounterConfigResponse(xyzResponseCode.SUCCESS.getCode(),
                    xyzResponseMessage.AGENTCOUNTER_NOTEXISTS.getMessage(), "", "", false);
        }

    }

    private com.taboor.qms.core.model.Ticket saveTicket(Ticket ticket) throws TaboorQMSServiceException, Exception {

        com.taboor.qms.core.model.Ticket ticketEntity = RestUtil.getRequestNoCheck(
                dbConnectorMicroserviceURL + "/tab/tickets/get/id?ticketId=" + ticket.getTicketId(),
                com.taboor.qms.core.model.Ticket.class);
        if (ticketEntity == null) {
            ticketEntity = new com.taboor.qms.core.model.Ticket();
        }

        if (ticket.getTransferedCounterId() != null) {
            ticketEntity.setTransferedCounter(getBranchCounter(ticket.getTransferedCounterId()));
        } else {
            ticketEntity.setTransferedCounter(null);
        }

        ticketEntity.setService(getServiceTAB(ticket.getServiceId()));
        ticketEntity.setBranch(getBranch(ticket.getBranchId()));
        ticketEntity.setAgentCallTime(ticket.getAgentCallTime());
        ticketEntity.setCounterNumber(ticket.getCounterNumber());
        ticketEntity.setCreatedTime(ticket.getCreatedTime());
        ticketEntity.setPositionTime(ticket.getPositionTime());
        ticketEntity.setTicketNumber(ticket.getTicketNumber());
        ticketEntity.setTicketStatus(ticket.getTicketStatus());
        ticketEntity.setTicketType(ticket.getTicketType());
        ticketEntity.setWaitingTime(ticket.getWaitingTime());
        ticketEntity.setTransferedStatus(ticket.getTransferedStatus());
        ticketEntity.setTicketId(ticket.getTicketId());

        HttpEntity<com.taboor.qms.core.model.Ticket> ticketEntityResponse = new HttpEntity<>(ticketEntity,
                HttpHeadersUtils.getApplicationJsonHeader());
        com.taboor.qms.core.model.Ticket createTicketResponse = RestUtil.postRequestEntity(
                dbConnectorMicroserviceURL + "/tab/tickets/save", ticketEntityResponse,
                com.taboor.qms.core.model.Ticket.class, xyzErrorMessage.TICKET_NOT_CREATED.getErrorCode(),
                xyzErrorMessage.TICKET_NOT_CREATED.getErrorDetails());
        return createTicketResponse;
    }

    private ServiceTAB getServiceTAB(@Valid Long serviceId) throws TaboorQMSServiceException, Exception {
        return RestUtil.getRequest(dbConnectorMicroserviceURL + "/tab/services/get/id?serviceId=" + serviceId,
                ServiceTAB.class, xyzErrorMessage.SERVICE_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorDetails());
    }

    private Branch getBranch(@Valid Long branchId) throws TaboorQMSServiceException, Exception {
        return RestUtil.getRequest(dbConnectorMicroserviceURL + "/tab/branches/get/id?branchId=" + branchId,
                Branch.class, xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.BRANCH_NOT_EXISTS.getErrorDetails());
    }

    private BranchCounter getBranchCounter(@Valid Long branchCounterId) throws TaboorQMSServiceException, Exception {
        return RestUtil.getRequest(
                dbConnectorMicroserviceURL + "/tab/branchcounters/get/id?branchCounterId=" + branchCounterId,
                BranchCounter.class, xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorCode(),
                xyzErrorMessage.COUNTER_NOT_EXISTS.getErrorDetails());
    }

    private int getNoOfTicketsInQueue(Long branchId, Long serviceId, Long counterId) {

        System.out.println("Params BranchId -->>" + branchId);
        System.out.println("Params ServiceId -->>" + serviceId);

        int ticketsInQueue = queueTicketMapperRepository.countQueueTicketsNotTransferedByCounterId(branchId, serviceId, counterId);

        System.out.println("No of Tickets In Queue -->>" + ticketsInQueue);

        return ticketsInQueue;
    }

}
