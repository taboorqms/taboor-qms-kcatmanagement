package com.taboor.qms.kcat.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.kcat.payload.AddCounterConfigPayload;
import com.taboor.qms.kcat.payload.AddTVConfigPayload;
import com.taboor.qms.kcat.payload.ChangeCounterStatusPayload;
import com.taboor.qms.kcat.payload.GetAllCounterTicketsPayload;
import com.taboor.qms.kcat.payload.GetCounterTicketPayload;
import com.taboor.qms.kcat.payload.NewCounterPayload;
import com.taboor.qms.kcat.payload.TicketChangeCounterNotifPayload;
import com.taboor.qms.kcat.response.AddCounterConfigResponse;
import com.taboor.qms.kcat.response.GetBranchAdminCounterListResponse;
import com.taboor.qms.kcat.response.GetCountersResponse;
import com.taboor.qms.kcat.response.GetTicketCounterResponse;
import com.taboor.qms.kcat.service.KcatService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin(origins = "*")
public class KcatController {

	private static final Logger logger = LoggerFactory.getLogger(KcatController.class);

	@Autowired
	KcatService kcatService;

	@ApiOperation(value = "Get All Branches Counters of BranchAdmin.", response = List.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "All Branches Counters fetched Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/admin/get/branches", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<GetBranchAdminCounterListResponse> getAllServices() throws Exception, Throwable {
		logger.info("Calling Get All Branches of Branch Admin - API");
		return kcatService.getBranchByEmp();
	}

	@ApiOperation(value = "Add Branch Counter Config", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Add Branch Counter Config Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/counter/add/config", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody AddCounterConfigResponse addCounterConfig(
			@RequestBody AddCounterConfigPayload addCounterConfigPayload) throws Exception, Throwable {
		logger.info("Calling Add Branch Counter Config - API");
		return kcatService.addCounterConfig(addCounterConfigPayload);
	}

	@PostMapping(value = "/counter/logout")
	public @ResponseBody GenStatusResponse logoutCounter() throws Exception, Throwable {
		logger.info("Calling Logout Counter - API");
		return kcatService.logoutCounter();
	}

	@PostMapping(value = "/tv/logout")
	public @ResponseBody GenStatusResponse logoutTV() throws Exception, Throwable {
		logger.info("Calling Logout TV - API");
		return kcatService.logoutTV();
	}

	@ApiOperation(value = "Get Ticket Counter Serving", response = GetTicketCounterResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Get Counter Ticket Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/counter/get/ticket", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetTicketCounterResponse getCounterTicket(@RequestBody GetCounterTicketPayload payload)
			throws Exception, Throwable {
		logger.info("Calling Get Ticket Counter Serving - API");
		return kcatService.getCounterTicket(payload);
	}

	@ApiOperation(value = "Get All Tickets of All Counters Serving in Branch", response = List.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Get All Counters Tickets Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/tv/get/tickets", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetCountersResponse getAllCounterTickets(@RequestBody GetAllCounterTicketsPayload payload)
			throws Exception, Throwable {
		logger.info("Get All Tickets of All Counters Serving in Branch - API");
		return kcatService.getAllCounterTickets(payload);
	}

	@ApiOperation(value = "Add Branch TV Config", response = GenStatusResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Add Branch TV Config Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/tv/add/config", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse addTvConfig(@RequestBody AddTVConfigPayload addTvConfigPayload)
			throws Exception, Throwable {
		logger.info("Calling Add Branch TV Config - API");
		return kcatService.addTvConfig(addTvConfigPayload);
	}

	// Test Notifications (to be removed)

	@RequestMapping(value = "/notif/send/change/ticket", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse sendTicketChange(@RequestBody GetCounterTicketPayload payload)
			throws Exception, Throwable {
		logger.info("Calling sending ticket serving & counter no - API");
		return kcatService.sendTicketChange(payload);
	}

	@RequestMapping(value = "/notif/send/get/feedback", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse sendFeedbackGet(@RequestBody GetCounterTicketPayload payload)
			throws Exception, Throwable {
		logger.info("Calling sending feedback after serving - API");
		return kcatService.sendFeedbackGet(payload);
	}

	@RequestMapping(value = "/notif/send/tv/change/ticket", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse sendTicketChangeTv(@RequestBody TicketChangeCounterNotifPayload payload)
			throws Exception, Throwable {
		logger.info("Calling sending ticket changed on counter TV - API");
		return kcatService.sendTicketChangeTV(payload);
	}

	@RequestMapping(value = "/notif/send/counter/change/agent/status", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse sendTicketChangeTv(
			@RequestBody ChangeCounterStatusPayload counterStatusPayload) throws Exception, Throwable {
		logger.info("Calling sending agent changed login status - API");
		return kcatService.sendAgentChangeLoginStatus(counterStatusPayload);
	}

	@RequestMapping(value = "/notif/send/new/counter/add", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse newCounterAdded(@RequestBody NewCounterPayload payload)
			throws Exception, Throwable {
		logger.info("Calling new counter added notification - API");
		return kcatService.sendNewCounterAdded(payload);
	}

}
