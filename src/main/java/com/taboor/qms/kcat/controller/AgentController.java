package com.taboor.qms.kcat.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.payload.GetActivitiesPayload;
import com.taboor.qms.core.payload.StepInOutTicketPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetIdListResponse;
import com.taboor.qms.core.response.GetStatisticsResponse;
import com.taboor.qms.kcat.payload.AddAgentCounterConfigPayload;
import com.taboor.qms.kcat.payload.LoginAgentPayload;
import com.taboor.qms.kcat.payload.NewCounterPayload;
import com.taboor.qms.kcat.response.AddAgentCounterConfigResponse;
import com.taboor.qms.kcat.response.GetBranchServicesResponse;
import com.taboor.qms.kcat.response.GetTicketServingResponse;
import com.taboor.qms.kcat.response.GetTransferCountersResponse;
import com.taboor.qms.kcat.response.LoginUserResponse;
import com.taboor.qms.kcat.service.AgentService;

import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/agent")
public class AgentController {

	private static final Logger logger = LoggerFactory.getLogger(AgentController.class);

	@Autowired
	AgentService agentService;

	@ApiOperation(value = "Add Agent Counter Config", response = GenStatusResponse.class)
	@RequestMapping(value = "/add/config", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody AddAgentCounterConfigResponse addCounterConfig(
			@RequestBody AddAgentCounterConfigPayload addAgentCounterConfigPayload) throws Exception, Throwable {
		logger.info("Calling Add Agent Counter Config - API");
		return agentService.addAgentCounterConfig(addAgentCounterConfigPayload);
	}

	@CrossOrigin
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public @ResponseBody LoginUserResponse loginAgent(@RequestBody @Valid final LoginAgentPayload loginAgentPayload,
			HttpServletRequest request) throws Exception, Throwable {
		logger.info("Calling Login Agent - API");
		loginAgentPayload.setIpAddress(request.getRemoteAddr());
		return agentService.loginAgent(loginAgentPayload);
	}

	@CrossOrigin
	@RequestMapping(value = "/get/profile", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody GetBranchServicesResponse getAgentProfile() throws Exception, Throwable {
		logger.info("Calling Get Agent Profile Details - API");
		return agentService.getBranchServicesDetails();
	}

	@CrossOrigin
	@RequestMapping(value = "/serve/ticket", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetTicketServingResponse serveTicket() throws Exception, Throwable {
		logger.info("Calling Serve Customer Ticket by Agent - API");
		return agentService.serveCustomerTicket();
	}

	@CrossOrigin
	@RequestMapping(value = "/next/ticket", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetTicketServingResponse nextTicket(
			@RequestBody @Valid StepInOutTicketPayload nextTicketPayload) throws Exception, Throwable {
		logger.info("Calling Next Customer Ticket by Agent - API");
		return agentService.callNextCustomerTicket(nextTicketPayload);
	}

	@RequestMapping(value = "/cancel/ticket", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse cancelTicket(@RequestBody @Valid StepInOutTicketPayload cancelTicketPayload)
			throws Exception, Throwable {
		logger.info("Calling Cancel Customer Ticket - API");
		return agentService.cancelCustomerTicket(cancelTicketPayload);
	}

	@RequestMapping(value = "/get/transfer/counters", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetTransferCountersResponse getTicketTransferCounters(
			@RequestBody @Valid StepInOutTicketPayload payload) throws Exception, Throwable {
		logger.info("Calling Get Ticket Transferable Counters by Agent - API");
		return agentService.getTransferCounters(payload);
	}

	@RequestMapping(value = "/transfer/ticket", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse transferTicket(@RequestBody @Valid GetIdListResponse payload)
			throws Exception, Throwable {
		logger.info("Calling Ticket Transfer To Selected Counter by Agent - API");
		return agentService.transferTicketCounter(payload);
	}

	@CrossOrigin
	@RequestMapping(value = "/recall/ticket", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse recallTicket(@RequestBody @Valid GetIdListResponse payload)
			throws Exception, Throwable {
		logger.info("Calling Recall Ticket by Agent - API");
		return agentService.recallTicket(payload);
	}

	@CrossOrigin
	@RequestMapping(value = "/logout", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse logoutAgent() throws Exception, Throwable {
		logger.info("Calling logout Agent - API");
		return agentService.logoutAgent();
	}

	@CrossOrigin
	@RequestMapping(value = "/getDetails", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse getAgentDetails() throws Exception, Throwable {
		logger.info("Calling Get Agents Details - API");
		return agentService.getDetails();
	}

	@CrossOrigin
	@RequestMapping(value = "/getStatistics", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GetStatisticsResponse getAgentStatistics(@RequestBody GetActivitiesPayload payload)
			throws Exception, Throwable {
		logger.info("Calling Get Agents Statistics - API");
		return agentService.getStatistics(payload);
	}

	@CrossOrigin
	@RequestMapping(value = "/getTicket/summary", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse getTicketSummary() throws Exception, Throwable {
		logger.info("Calling Get Count of Tickets By Agent (Ticket Summary) - API");
		return agentService.getAgentTicketsSummary();
	}
	
	@CrossOrigin
	@RequestMapping(value = "/check/config", method = RequestMethod.POST, 
			consumes = "application/json", produces = "application/json")
	public @ResponseBody GenStatusResponse checkConfiguration(@RequestBody @Valid final NewCounterPayload payload) throws Exception, Throwable {
		logger.info("Calling Check Configuration By Agent - API");
//		logger.info("PAyload --->> "+payload.getDeviceToken());
		return agentService.checkConfiguration(payload);
	}
	
	//test notifications
	@CrossOrigin
	@RequestMapping(value = "/notif/send/number/queue/tickets", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenStatusResponse noQueueTickets(@RequestBody NewCounterPayload payload)
			throws Exception, Throwable {
		logger.info("Calling number of tickets in queue remaining notification - API");
		return agentService.sendNoOfTicketsInQueueNotif(payload);
	}

}
