package com.taboor.qms.kcat.response;

import java.util.List;

import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.utils.PrivilegeName;

public class LoginUserResponse extends GenStatusResponse {

	private String sessionToken;
	private String refreshToken;
	private String role;
	private Boolean setup;
	private List<PrivilegeName> privileges;

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * @return the setup
	 */
	public Boolean getSetup() {
		return setup;
	}

	/**
	 * @param setup the setup to set
	 */
	public void setSetup(Boolean setup) {
		this.setup = setup;
	}

	public List<PrivilegeName> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(List<PrivilegeName> privileges) {
		this.privileges = privileges;
	}

	public LoginUserResponse() {
		super();
	}

	public LoginUserResponse(int applicationStatusCode, String applicationStatusResponse, String sessionToken,
			String refreshToken, String role, Boolean setup, List<PrivilegeName> privileges) {
		super(applicationStatusCode, applicationStatusResponse);
		this.sessionToken = sessionToken;
		this.refreshToken = refreshToken;
		this.role = role;
		this.setup = setup;
		this.privileges = privileges;
	}

}
