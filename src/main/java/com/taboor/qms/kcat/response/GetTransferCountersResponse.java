package com.taboor.qms.kcat.response;

import java.util.List;

import com.taboor.qms.core.model.BranchCounter;
import com.taboor.qms.core.response.GenStatusResponse;

public class GetTransferCountersResponse extends GenStatusResponse {

	private List<BranchCounter> counters;
 		
	public GetTransferCountersResponse () {
		
	}

	public List<BranchCounter> getCounters() {
		return counters;
	}

	public void setCounters(List<BranchCounter> counters) {
		this.counters = counters;
	}

	public GetTransferCountersResponse(int applicationStatusCode, String applicationStatusResponse, 
			List<BranchCounter> counters) {
		super(applicationStatusCode, applicationStatusResponse);
		this.counters = counters;
	}

}
