package com.taboor.qms.kcat.response;

import java.util.List;

import com.taboor.qms.core.response.GenStatusResponse;

public class GetBranchAdminCounterListResponse extends GenStatusResponse {

	public GetBranchAdminCounterListResponse(List<BranchAdminCounter> counterList, int applicationStatusCode,
			String applicationStatusResponse) {
		super(applicationStatusCode, applicationStatusResponse);
		this.counterList = counterList;
	}

	public GetBranchAdminCounterListResponse() {

	}

	private Long branchId;
	private String branchName;
	private String branchNameArabic;
	private String emailAddress;
	private List<BranchAdminCounter> counterList;

	public class BranchAdminCounter {

		private Long counterId;
		private String counterNumber;
		private Boolean setup;

		public Long getCounterId() {
			return counterId;
		}

		public void setCounterId(Long counterId) {
			this.counterId = counterId;
		}

		public String getCounterNumber() {
			return counterNumber;
		}

		public void setCounterNumber(String counterNumber) {
			this.counterNumber = counterNumber;
		}

		public Boolean getSetup() {
			return setup;
		}

		public void setSetup(Boolean setup) {
			this.setup = setup;
		}

	}

	public Long getBranchId() {
		return branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchNameArabic() {
		return branchNameArabic;
	}

	public void setBranchNameArabic(String branchNameArabic) {
		this.branchNameArabic = branchNameArabic;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public List<BranchAdminCounter> getCounterList() {
		return counterList;
	}

	public void setCounterList(List<BranchAdminCounter> counterList) {
		this.counterList = counterList;
	}

}
