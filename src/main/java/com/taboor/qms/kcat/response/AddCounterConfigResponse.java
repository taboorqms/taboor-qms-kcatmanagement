package com.taboor.qms.kcat.response;

import com.taboor.qms.core.response.GenStatusResponse;

public class AddCounterConfigResponse extends GenStatusResponse {

	private boolean agentLoginStatus;

	public boolean isAgentLoginStatus() {
		return agentLoginStatus;
	}

	public void setAgentLoginStatus(boolean agentLoginStatus) {
		this.agentLoginStatus = agentLoginStatus;
	}

	public AddCounterConfigResponse(int applicationStatusCode, String applicationStatusResponse, boolean agentLoginStatus) {
		super(applicationStatusCode, applicationStatusResponse);
		this.agentLoginStatus = agentLoginStatus;
	}

}
