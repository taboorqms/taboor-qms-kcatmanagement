package com.taboor.qms.kcat.response;

import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.kcat.model.Ticket;

public class GetTicketServingResponse extends GenStatusResponse {

	private Ticket ticket;
	private int ticketsWaitingInQueue;
	private String customerName;
	private String customerEmail;
	private String customerContact;
	
	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public int getTicketsWaitingInQueue() {
		return ticketsWaitingInQueue;
	}

	public void setTicketsWaitingInQueue(int ticketsWaitingInQueue) {
		this.ticketsWaitingInQueue = ticketsWaitingInQueue;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerContact() {
		return customerContact;
	}

	public void setCustomerContact(String customerContact) {
		this.customerContact = customerContact;
	}

	public GetTicketServingResponse(int applicationStatusCode, String applicationStatusResponse, Ticket ticket,
			int ticketsWaitingInQueue, String customerName, String customerEmail, String customerContact) {
		super(applicationStatusCode, applicationStatusResponse);
		this.ticket = ticket;
		this.ticketsWaitingInQueue = ticketsWaitingInQueue;
		this.customerName = customerName;
		this.customerEmail = customerEmail;
		this.customerContact = customerContact;
	}

}
