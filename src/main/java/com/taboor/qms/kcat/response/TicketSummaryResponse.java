package com.taboor.qms.kcat.response;

import com.taboor.qms.core.response.GenStatusResponse;

public class TicketSummaryResponse extends GenStatusResponse {

	private int totalTicketServed;
	private int totalTicketCancelled;
	private int totalTicketTransferred;
	private int totalTicketRecalled;
	private int totalSum;

	public int getTotalTicketServed() {
		return totalTicketServed;
	}

	public void setTotalTicketServed(int totalTicketServed) {
		this.totalTicketServed = totalTicketServed;
	}

	public int getTotalTicketCancelled() {
		return totalTicketCancelled;
	}

	public void setTotalTicketCancelled(int totalTicketCancelled) {
		this.totalTicketCancelled = totalTicketCancelled;
	}

	public int getTotalTicketTransferred() {
		return totalTicketTransferred;
	}

	public void setTotalTicketTransferred(int totalTicketTransferred) {
		this.totalTicketTransferred = totalTicketTransferred;
	}

	public int getTotalTicketRecalled() {
		return totalTicketRecalled;
	}

	public void setTotalTicketRecalled(int totalTicketRecalled) {
		this.totalTicketRecalled = totalTicketRecalled;
	}

	public int getTotalSum() {
		return totalSum;
	}

	public void setTotalSum(int totalSum) {
		this.totalSum = totalSum;
	}

	public TicketSummaryResponse(int applicationStatusCode, String applicationStatusResponse, int totalTicketServed,
			int totalTicketCancelled, int totalTicketTransferred,	int totalTicketRecalled) {
		super(applicationStatusCode, applicationStatusResponse);
		this.totalTicketServed = totalTicketServed;
		this.totalTicketCancelled = totalTicketCancelled;
		this.totalTicketTransferred = totalTicketTransferred;
		this.totalTicketRecalled = totalTicketRecalled;
		this.totalSum = totalTicketServed+totalTicketCancelled+totalTicketTransferred;
	}

}
