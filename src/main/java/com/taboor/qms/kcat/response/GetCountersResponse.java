package com.taboor.qms.kcat.response;

import java.util.List;

import com.taboor.qms.core.response.GenStatusResponse;

public class GetCountersResponse extends GenStatusResponse {

	private List<GetAllCounters> counters;
 	
	public class GetAllCounters {

		private String counterNumber;
		private boolean counterStatus;
		private String ticketNumber;
		
		public String getCounterNumber() {
			return counterNumber;
		}
		public void setCounterNumber(String counterNumber) {
			this.counterNumber = counterNumber;
		}
		public boolean isCounterStatus() {
			return counterStatus;
		}
		public void setCounterStatus(boolean counterStatus) {
			this.counterStatus = counterStatus;
		}
		public String getTicketNumber() {
			return ticketNumber;
		}
		public void setTicketNumber(String ticketNumber) {
			this.ticketNumber = ticketNumber;
		}
	}
	
	public GetCountersResponse () {
		
	}

	public List<GetAllCounters> getCounters() {
		return counters;
	}

	public void setCounters(List<GetAllCounters> counters) {
		this.counters = counters;
	}


	public GetCountersResponse(int applicationStatusCode, String applicationStatusResponse, List<GetAllCounters> counters) {
		super(applicationStatusCode, applicationStatusResponse);
		this.counters = counters;
	}

}
