package com.taboor.qms.kcat.response;

import java.util.List;

import com.taboor.qms.core.model.ServiceRequirement;
import com.taboor.qms.core.response.GenStatusResponse;

public class GetBranchServicesResponse extends GenStatusResponse {

	private String branchName;
	private String branchLocation;
	private String counterNumber;
	private String agentName;
	private String serviceCentrePic;

	private List<ServiceDetails> serviceList;

	public class ServiceDetails {
		private String serviceName;
		private List<ServiceRequirement> requirements;

		public String getServiceName() {
			return serviceName;
		}

		public void setServiceName(String serviceName) {
			this.serviceName = serviceName;
		}

		public List<ServiceRequirement> getRequirements() {
			return requirements;
		}

		public void setRequirements(List<ServiceRequirement> requirements) {
			this.requirements = requirements;
		}

	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchLocation() {
		return branchLocation;
	}

	public void setBranchLocation(String branchLocation) {
		this.branchLocation = branchLocation;
	}

	public String getCounterNumber() {
		return counterNumber;
	}

	public void setCounterNumber(String counterNumber) {
		this.counterNumber = counterNumber;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getServiceCentrePic() {
		return serviceCentrePic;
	}

	public void setServiceCentrePic(String serviceCentrePic) {
		this.serviceCentrePic = serviceCentrePic;
	}

	public List<ServiceDetails> getServiceList() {
		return serviceList;
	}

	public void setServiceList(List<ServiceDetails> serviceList) {
		this.serviceList = serviceList;
	}

	public GetBranchServicesResponse() {
		// TODO Auto-generated constructor stub
	}

	public GetBranchServicesResponse(int applicationStatusCode, String applicationStatusResponse, String branchName,
			String branchLocation, String counterNumber, String agentName, String serviceCentrePic ,List<ServiceDetails> serviceList) {
		super(applicationStatusCode, applicationStatusResponse);
		this.branchName = branchName;
		this.branchLocation = branchLocation;
		this.counterNumber = counterNumber;
		this.agentName = agentName;
		this.serviceCentrePic = serviceCentrePic;
		this.serviceList = serviceList;
	}

}
