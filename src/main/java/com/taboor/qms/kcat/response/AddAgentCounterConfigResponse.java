package com.taboor.qms.kcat.response;

import com.taboor.qms.core.response.GenStatusResponse;

public class AddAgentCounterConfigResponse extends GenStatusResponse {

	private String branchId;
	private String counterNumber;
	private Boolean config;
	
	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public String getCounterNumber() {
		return counterNumber;
	}

	public void setCounterNumber(String counterNumber) {
		this.counterNumber = counterNumber;
	}

	public Boolean getConfig() {
		return config;
	}

	public void setConfig(Boolean config) {
		this.config = config;
	}

	public AddAgentCounterConfigResponse(int applicationStatusCode, String applicationStatusResponse,
			String branchId, String counterNumber, Boolean config) {
		super(applicationStatusCode, applicationStatusResponse);
		this.branchId = branchId;
		this.counterNumber = counterNumber;
		this.config = config;
	}

}
