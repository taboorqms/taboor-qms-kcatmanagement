package com.taboor.qms.kcat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.taboor.qms.core.auth.JwtAuthenticationFilter;
import com.taboor.qms.core.utils.PrivilegeName;

@EnableWebSecurity
class AuthWebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService myUserDetailsService;

	@Autowired
	private JwtAuthenticationFilter jwtAuthenticationFilter;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(myUserDetailsService);
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}

	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.csrf().disable().authorizeRequests().antMatchers(HttpMethod.OPTIONS).permitAll()
				.antMatchers("/h2-console/**", "/swagger-ui.html", "/webjars/**", "/swagger-resources/**", "/v2/**")
				.permitAll().antMatchers("/admin/get/branches").hasAnyRole(PrivilegeName.Branch_Managment.name())
				.antMatchers("/counter/add/config").hasAnyRole(PrivilegeName.Branch_Managment.name())
				.antMatchers("/counter/get/ticket").hasAnyRole(PrivilegeName.Branch_Managment.name())
				.antMatchers("/tv/add/config")
				.hasAnyRole(PrivilegeName.Branch_Managment.name()).antMatchers("/tv/get/tickets")
				.hasAnyRole(PrivilegeName.Branch_Managment.name()).antMatchers("/settings/update").hasAnyRole()
				.antMatchers("/agent/add/config").hasAnyRole(PrivilegeName.Branch_Managment.name())
				.antMatchers("/agent/get/profile").hasAnyRole(PrivilegeName.Agent_Serve.name(), PrivilegeName.Branch_Managment.name())
				.antMatchers("/agent/serve/ticket").hasAnyRole(PrivilegeName.Agent_Serve.name(), PrivilegeName.Branch_Managment.name())
				.antMatchers("/agent/next/ticket").hasAnyRole(PrivilegeName.Agent_Serve.name(), PrivilegeName.Branch_Managment.name())
				.antMatchers("/agent/cancel/ticket").hasAnyRole(PrivilegeName.Agent_Serve.name(), PrivilegeName.Branch_Managment.name())
				.antMatchers("/agent/get/transfer/counters").hasAnyRole(PrivilegeName.Agent_Serve.name(), PrivilegeName.Branch_Managment.name())
				.antMatchers("/agent/transfer/ticket").hasAnyRole(PrivilegeName.Agent_Serve.name(), PrivilegeName.Branch_Managment.name())
				.antMatchers("/agent/recall/ticket").hasAnyRole(PrivilegeName.Agent_Serve.name(), PrivilegeName.Branch_Managment.name())
				.antMatchers("/agent/logout").hasAnyRole(PrivilegeName.Agent_Serve.name(), PrivilegeName.Branch_Managment.name())
				.antMatchers("/agent/getDetails").hasAnyRole(PrivilegeName.Agent_Serve.name(), PrivilegeName.Branch_Managment.name())
				.antMatchers("/agent/getStatistics").hasAnyRole(PrivilegeName.Agent_Serve.name(), PrivilegeName.Branch_Managment.name())
				.antMatchers("/agent/getTicket/summary").hasAnyRole(PrivilegeName.Agent_Serve.name(), PrivilegeName.Branch_Managment.name())
				.antMatchers("/queue/tickets/get").hasAnyRole(PrivilegeName.Agent_Serve.name(), PrivilegeName.Branch_Managment.name())
				.antMatchers("/notification/push").hasAnyRole(PrivilegeName.Agent_Serve.name(), PrivilegeName.Branch_Managment.name())
				.antMatchers("/ticketFastpass/fastpass/cancel").hasAnyRole(PrivilegeName.Agent_Serve.name(), PrivilegeName.Branch_Managment.name())
				.antMatchers("/counter/logout").hasAnyRole(PrivilegeName.Branch_Managment.name())
				.antMatchers("/tv/logout").hasAnyRole(PrivilegeName.Branch_Managment.name())

				// ByPass Routes
				.antMatchers("/agent/login", "/agent/check/config", 
						"/agent/notif/send/number/queue/tickets", "/notif/send/new/counter/add",
						"/notif/send/counter/change/agent/status", "/notif/send/tv/change/ticket",
						"/notif/send/get/feedback", "/notif/send/change/ticket").permitAll().anyRequest().denyAll();
		httpSecurity.headers().frameOptions().disable();
		httpSecurity.addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);

	}

}