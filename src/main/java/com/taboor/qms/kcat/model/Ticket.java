package com.taboor.qms.kcat.model;

import java.time.LocalTime;
import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "tab_ticket")
public class Ticket implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Long ticketId;
	private String ticketNumber;
	private int ticketType;
	private OffsetDateTime createdTime;
	private OffsetDateTime positionTime;
	private OffsetDateTime agentCallTime;
	private int ticketStatus;
	private LocalTime waitingTime;
	private String counterNumber;

	private Long branchId;
	private Long serviceId;
	private String serviceName;
	private int transferedStatus;
	private Long transferedCounterId;
	private Boolean stepOut;
	private int stepOutPosition;

	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ticket_id", unique = true, nullable = false)
	public Long getTicketId() {
		return ticketId;
	}

	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public int getTicketType() {
		return ticketType;
	}

	public void setTicketType(int ticketType) {
		this.ticketType = ticketType;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public OffsetDateTime getPositionTime() {
		return positionTime;
	}

	public void setPositionTime(OffsetDateTime positionTime) {
		this.positionTime = positionTime;
	}

	public OffsetDateTime getAgentCallTime() {
		return agentCallTime;
	}

	public void setAgentCallTime(OffsetDateTime agentCallTime) {
		this.agentCallTime = agentCallTime;
	}

	public int getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(int ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	@Transient
	public LocalTime getWaitingTime() {
		return waitingTime;
	}

	public void setWaitingTime(LocalTime waitingTime) {
		this.waitingTime = waitingTime;
	}

	public String getCounterNumber() {
		return counterNumber;
	}

	public void setCounterNumber(String counterNumber) {
		this.counterNumber = counterNumber;
	}

	public int getTransferedStatus() {
		return transferedStatus;
	}

	public void setTransferedStatus(int transferedStatus) {
		this.transferedStatus = transferedStatus;
	}

	public Long getBranchId() {
		return branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public Long getServiceId() {
		return serviceId;
	}

	public void setServiceId(Long serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public Long getTransferedCounterId() {
		return transferedCounterId;
	}

	public void setTransferedCounterId(Long transferedCounterId) {
		this.transferedCounterId = transferedCounterId;
	}

	public Boolean getStepOut() {
		return stepOut;
	}

	public void setStepOut(Boolean stepOut) {
		this.stepOut = stepOut;
	}

	@Column(columnDefinition = "int default 0")
	public int getStepOutPosition() {
		return stepOutPosition;
	}

	public void setStepOutPosition(int stepOutPosition) {
		this.stepOutPosition = stepOutPosition;
	}

}
