package com.taboor.qms.kcat.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "tab_queue_ticket_mapper")
public class QueueTicketMapper implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Long queueTicketId;
	private Queue queue;
	private Ticket ticket;

	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "queue_ticket_id", unique = true, nullable = false)
	public Long getQueueTicketId() {
		return queueTicketId;
	}

	public void setQueueTicketId(Long queueTicketId) {
		this.queueTicketId = queueTicketId;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "qtm_queue_id", nullable = false)
	public Queue getQueue() {
		return queue;
	}

	public void setQueue(Queue queue) {
		this.queue = queue;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "qtm_ticket_id", nullable = false)
	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

}
