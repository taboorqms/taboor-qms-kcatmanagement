package com.taboor.qms.kcat;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

@SpringBootApplication
@ComponentScan({ "com.taboor.qms.kcat", "com.taboor.qms.core" })
@EntityScan("com.taboor.qms.kcat.model")
@EnableJpaRepositories(basePackages = { "com.taboor.qms.kcat.repository" })
public class TaboorQmsKCATManagementApplication implements CommandLineRunner {

	@PersistenceContext
	EntityManager em;

//	@Autowired
//	MatcherRepository matcherRepository;
//
//	@Autowired
//	PrivilegeRepository privilegeRepository;

	public static void main(String[] args) {
		SpringApplication.run(TaboorQmsKCATManagementApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		FirebaseOptions options = new FirebaseOptions.Builder()
				.setCredentials(GoogleCredentials.fromStream(getClass().getClassLoader()
						.getResourceAsStream("taboor-firebase-adminsdk-0p6y8-b25b763338.json")))
				.setDatabaseUrl("https://taboor.firebaseio.com").build();

		FirebaseApp.initializeApp(options);
	}

//	@PostConstruct
//	@Transactional
//	void dumpIntoDb() {
//
//		List<Privilege> savedPrivileges = privilegeRepository.findAll();
//
//		List<Matcher> matchers = saveMatcherList(new MatcherObject("/admin/get/branches", HttpMethod.GET),
//				new MatcherObject("/counter/add/config", HttpMethod.POST),
//				new MatcherObject("/counter/get/ticket", HttpMethod.POST),
//				new MatcherObject("/notif/send/change/ticket", HttpMethod.POST),
//				new MatcherObject("/notif/send/get/feedback", HttpMethod.POST),
//				new MatcherObject("/notif/send/tv/change/ticket", HttpMethod.POST),
//				new MatcherObject("/notif/send/counter/change/agent/status", HttpMethod.POST),
//				new MatcherObject("/notif/send/new/counter/add", HttpMethod.POST),
//				new MatcherObject("/agent/notif/send/number/queue/tickets", HttpMethod.POST),
//				new MatcherObject("/tv/add/config", HttpMethod.POST),
//				new MatcherObject("/tv/get/tickets", HttpMethod.POST),
//				new MatcherObject("/settings/update", HttpMethod.POST),
//				new MatcherObject("/agent/add/config", HttpMethod.POST),
//				new MatcherObject("/agent/get/profile", HttpMethod.POST),
//				new MatcherObject("/agent/serve/ticket", HttpMethod.POST),
//				new MatcherObject("/agent/next/ticket", HttpMethod.POST),
//				new MatcherObject("/agent/cancel/ticket", HttpMethod.POST),
//				new MatcherObject("/agent/get/transfer/counters", HttpMethod.POST),
//				new MatcherObject("/agent/transfer/ticket", HttpMethod.POST),
//				new MatcherObject("/agent/recall/ticket", HttpMethod.POST),
//				new MatcherObject("/agent/logout", HttpMethod.POST),
//				new MatcherObject("/agent/getDetails", HttpMethod.POST),
//				new MatcherObject("/agent/getStatistics", HttpMethod.POST),
//				new MatcherObject("/agent/getTicket/summary", HttpMethod.POST),
//				new MatcherObject("/queue/tickets/get", HttpMethod.POST),
//				new MatcherObject("/notification/push", HttpMethod.POST),
//				new MatcherObject("/ticketFastpass/fastpass/cancel", HttpMethod.POST)
//
//		);
//
//		List<Privilege> privileges = new ArrayList<Privilege>();
//		Privilege privilege;
//
//		privilege = savedPrivileges.stream()
//				.filter(entity -> PrivilegeName.BRANCH_MANAGEMENT.equals(entity.getPrivilegeName())).findAny()
//				.orElse(new Privilege());
//		if (privilege.getPrivilegeName() == null)
//			privilege.setPrivilegeName(PrivilegeName.BRANCH_MANAGEMENT);
//		privilege.getMatchers()
//				.addAll(getMatchers(matchers, "/admin/get/branches", "/counter/add/config", "/counter/get/ticket",
//						"/notif/send/change/ticket", "/notif/send/get/feedback", "/notif/send/tv/change/ticket",
//						"/notif/send/new/counter/add", "/tv/add/config", "/tv/get/tickets",
//						"/notif/send/counter/change/agent/status", "/agent/add/config", "/guest/get/branches"));
//		privileges.add(privilege);
//
//		privilege = savedPrivileges.stream()
//				.filter(entity -> PrivilegeName.AGENT_SERVE.equals(entity.getPrivilegeName())).findAny()
//				.orElse(new Privilege());
//		if (privilege.getPrivilegeName() == null)
//			privilege.setPrivilegeName(PrivilegeName.AGENT_SERVE);
//		privilege.getMatchers()
//				.addAll(getMatchers(matchers, "/agent/get/profile", "/agent/serve/ticket", "/agent/next/ticket",
//						"/agent/cancel/ticket", "/agent/get/transfer/counters", "/agent/transfer/ticket",
//						"/agent/recall/ticket", "/agent/logout", "/agent/getDetails", "/agent/getStatistics",
//						"/queue/tickets/get", "/notification/push", "/ticketFastpass/fastpass/cancel",
//						"/agent/getTicket/summary", "/agent/notif/send/number/queue/tickets"));
//		privileges.add(privilege);
//
//		privileges = privilegeRepository.saveAll(privileges);
//		System.out.println("Success");
//
//	}
//
//	private List<Matcher> saveMatcherList(MatcherObject... matchers) {
//		List<Matcher> matcherList = new ArrayList<Matcher>();
//		for (MatcherObject entity : matchers) {
//			Matcher matcher = new Matcher();
//			matcher.setName(entity.getMatcherName());
//			matcher.setMethodinfo(entity.getMethodType().name());
//			matcher.setMicroserviceName(MicroserviceName.KCAT_MANAGEMENT);
//			matcherList.add(matcher);
//		}
//		return matcherRepository.saveAll(matcherList);
//	}
//
//	private Collection<Matcher> getMatchers(List<Matcher> matcherList, String... matcherNames) {
//		List<String> assignMatcherList = Arrays.asList(matcherNames);
//		List<Matcher> matchers = new ArrayList<Matcher>();
//		assignMatcherList.forEach(name -> matchers
//				.add(matcherList.stream().filter(entity -> name.equals(entity.getName())).findAny().orElse(null)));
//		return matchers;
//	}

}
