package com.taboor.qms.kcat.service;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.payload.GetActivitiesPayload;
import com.taboor.qms.core.payload.StepInOutTicketPayload;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.response.GetAgentListResponse;
import com.taboor.qms.core.response.GetIdListResponse;
import com.taboor.qms.core.response.GetStatisticsResponse;
import com.taboor.qms.kcat.payload.AddAgentCounterConfigPayload;
import com.taboor.qms.kcat.payload.LoginAgentPayload;
import com.taboor.qms.kcat.payload.NewCounterPayload;
import com.taboor.qms.kcat.response.AddAgentCounterConfigResponse;
import com.taboor.qms.kcat.response.GetBranchServicesResponse;
import com.taboor.qms.kcat.response.GetTicketServingResponse;
import com.taboor.qms.kcat.response.GetTransferCountersResponse;
import com.taboor.qms.kcat.response.LoginUserResponse;
import com.taboor.qms.kcat.response.TicketSummaryResponse;

@Service
public interface AgentService {

	public AddAgentCounterConfigResponse addAgentCounterConfig(AddAgentCounterConfigPayload addAgentCounterConfigPayload)
			throws TaboorQMSServiceException, Exception;
	
	public LoginUserResponse loginAgent(@Valid LoginAgentPayload loginAgentPayload)
			throws TaboorQMSServiceException, Exception;
	
	public GetBranchServicesResponse getBranchServicesDetails() throws TaboorQMSServiceException, Exception;
	
	public GetTicketServingResponse serveCustomerTicket() throws TaboorQMSServiceException, Exception;
	
	public GenStatusResponse cancelCustomerTicket(@Valid StepInOutTicketPayload cancelTicketPayload)
			throws TaboorQMSServiceException, Exception;
	
	public GetTicketServingResponse callNextCustomerTicket(@Valid StepInOutTicketPayload nextTicketPayload)
			throws TaboorQMSServiceException, Exception;
	
	public GetTransferCountersResponse getTransferCounters(@Valid StepInOutTicketPayload payload)
			throws TaboorQMSServiceException, Exception;
	
	public GenStatusResponse transferTicketCounter(@Valid GetIdListResponse payload)
			throws TaboorQMSServiceException, Exception;
	
	public GenStatusResponse recallTicket(@Valid GetIdListResponse payload)
			throws TaboorQMSServiceException, Exception;
	
	public GenStatusResponse logoutAgent() throws TaboorQMSServiceException, Exception;
	
	public GetAgentListResponse getDetails() throws TaboorQMSServiceException, Exception;
	
	public GetStatisticsResponse getStatistics(GetActivitiesPayload payload)
			throws TaboorQMSServiceException, Exception;

	public TicketSummaryResponse getAgentTicketsSummary() throws TaboorQMSServiceException, Exception;

	public AddAgentCounterConfigResponse checkConfiguration(NewCounterPayload payload) throws TaboorQMSServiceException, Exception;

	public GenStatusResponse sendNoOfTicketsInQueueNotif(NewCounterPayload payload)
			throws TaboorQMSServiceException, Exception;
}
