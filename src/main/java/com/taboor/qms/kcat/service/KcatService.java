package com.taboor.qms.kcat.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.kcat.payload.AddCounterConfigPayload;
import com.taboor.qms.kcat.payload.AddTVConfigPayload;
import com.taboor.qms.kcat.payload.ChangeCounterStatusPayload;
import com.taboor.qms.kcat.payload.GetAllCounterTicketsPayload;
import com.taboor.qms.kcat.payload.GetCounterTicketPayload;
import com.taboor.qms.kcat.payload.NewCounterPayload;
import com.taboor.qms.kcat.payload.TicketChangeCounterNotifPayload;
import com.taboor.qms.kcat.response.AddCounterConfigResponse;
import com.taboor.qms.kcat.response.GetBranchAdminCounterListResponse;
import com.taboor.qms.kcat.response.GetCountersResponse;
import com.taboor.qms.kcat.response.GetTicketCounterResponse;

@Service
public interface KcatService {

	public List<GetBranchAdminCounterListResponse> getBranchByEmp() throws TaboorQMSServiceException, Exception;

	public AddCounterConfigResponse addCounterConfig(AddCounterConfigPayload addCounterConfigPayload)
			throws TaboorQMSServiceException, Exception;

	public GetTicketCounterResponse getCounterTicket(GetCounterTicketPayload getCounterTicketPayload)
			throws TaboorQMSServiceException, Exception;

	public GenStatusResponse addTvConfig(AddTVConfigPayload addTvConfigPayload)
			throws TaboorQMSServiceException, Exception;

	public GetCountersResponse getAllCounterTickets(GetAllCounterTicketsPayload getAllCounterTicketsPayload)
			throws TaboorQMSServiceException, Exception;

	// Test Notifications (to be removed)

	public GenStatusResponse sendTicketChange(GetCounterTicketPayload getCounterTicketPayload)
			throws TaboorQMSServiceException, Exception;

	public GenStatusResponse sendFeedbackGet(GetCounterTicketPayload getCounterTicketPayload)
			throws TaboorQMSServiceException, Exception;

	public GenStatusResponse sendTicketChangeTV(TicketChangeCounterNotifPayload payload)
			throws TaboorQMSServiceException, Exception;

	public GenStatusResponse sendAgentChangeLoginStatus(ChangeCounterStatusPayload counterStatusPayload)
			throws TaboorQMSServiceException, Exception;

	public GenStatusResponse sendNewCounterAdded(NewCounterPayload payload) throws TaboorQMSServiceException, Exception;

	public GenStatusResponse logoutCounter() throws TaboorQMSServiceException, Exception;

	public GenStatusResponse logoutTV() throws TaboorQMSServiceException, Exception;

}
