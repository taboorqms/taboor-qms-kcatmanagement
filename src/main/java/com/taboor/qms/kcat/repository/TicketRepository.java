package com.taboor.qms.kcat.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.kcat.model.Ticket;


@Repository
@Transactional
public interface TicketRepository extends JpaRepository<Ticket, Long> {

	@Modifying
	@Query("DELETE from Ticket t where t.ticketId = :ticketId")
	int deleteByTicketId(@Param("ticketId") long ticketId);

	Optional<Ticket> findByTicketNumber(String ticketNumber);
}
