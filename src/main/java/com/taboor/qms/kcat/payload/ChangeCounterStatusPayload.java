package com.taboor.qms.kcat.payload;

public class ChangeCounterStatusPayload {

	private Long branchCounterId;
	private String deviceToken;
	private Boolean counterStatus;
	
	public Long getBranchCounterId() {
		return branchCounterId;
	}
	public void setBranchCounterId(Long branchCounterId) {
		this.branchCounterId = branchCounterId;
	}
	public String getDeviceToken() {
		return deviceToken;
	}
	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
	public Boolean getCounterStatus() {
		return counterStatus;
	}
	public void setCounterStatus(Boolean counterStatus) {
		this.counterStatus = counterStatus;
	}	
}
