package com.taboor.qms.kcat.payload;

public class AddGuestTicketPayload {

	private String ticketNumber;
	private String sessionToken;
//	private Long ticketId;

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

//	public Long getTicketId() {
//		return ticketId;
//	}
//
//	public void setTicketId(Long ticketId) {
//		this.ticketId = ticketId;
//	}

}
