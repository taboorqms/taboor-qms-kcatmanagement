package com.taboor.qms.kcat.payload;

public class NewCounterPayload {

	private String deviceToken;

	public String getDeviceToken() {
		return deviceToken;
	}
	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
		
}
