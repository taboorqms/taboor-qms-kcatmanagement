package com.taboor.qms.kcat.payload;

public class GetAllCounterTicketsPayload {

	private Long branchId;
	
	public Long getBranchId() {
		return branchId;
	}
	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}
		
}
