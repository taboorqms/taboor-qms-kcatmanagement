package com.taboor.qms.kcat.payload;

public class TicketChangeCounterNotifPayload {

	private String counterNumber;
	private String deviceToken;
	private Boolean counterStatus;
	private Boolean counterFlag;
	
	public String getCounterNumber() {
		return counterNumber;
	}
	public void setCounterNumber(String counterNumber) {
		this.counterNumber = counterNumber;
	}
	public String getDeviceToken() {
		return deviceToken;
	}
	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
	public Boolean getCounterStatus() {
		return counterStatus;
	}
	public void setCounterStatus(Boolean counterStatus) {
		this.counterStatus = counterStatus;
	}
	public Boolean getCounterFlag() {
		return counterFlag;
	}
	public void setCounterFlag(Boolean counterFlag) {
		this.counterFlag = counterFlag;
	}
		
}
