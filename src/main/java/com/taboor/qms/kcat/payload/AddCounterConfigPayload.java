package com.taboor.qms.kcat.payload;

public class AddCounterConfigPayload {

	private Long branchCounterId;
	private String deviceToken;
	private Boolean feedBackScreen;
	
	public Long getBranchCounterId() {
		return branchCounterId;
	}
	public void setBranchCounterId(Long branchCounterId) {
		this.branchCounterId = branchCounterId;
	}
	public String getDeviceToken() {
		return deviceToken;
	}
	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
	public Boolean getFeedBackScreen() {
		return feedBackScreen;
	}
	public void setFeedBackScreen(Boolean feedBackScreen) {
		this.feedBackScreen = feedBackScreen;
	}
}
