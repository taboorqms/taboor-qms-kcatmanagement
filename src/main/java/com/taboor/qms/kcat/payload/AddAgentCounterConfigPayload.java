package com.taboor.qms.kcat.payload;

public class AddAgentCounterConfigPayload {

	private Long branchCounterId;
	private String deviceToken;
	
	public Long getBranchCounterId() {
		return branchCounterId;
	}
	public void setBranchCounterId(Long branchCounterId) {
		this.branchCounterId = branchCounterId;
	}
	public String getDeviceToken() {
		return deviceToken;
	}
	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
}
